#!/bin/bash

if [ ${#} != 1 ]; then
    echo "usage: ${0} sample.filelist"
    exit -1
fi

INLIST=${1}
OUTLIST=${INLIST/AOD/NTUP_PILEUP}

if [ -e ${OUTLIST} ]; then
    rm ${OUTLIST}
fi

for i in $(cat ${INLIST})
do
    PRWDS=$(echo ${i} | cut -d . -f 1-3).merge.NTUP_PILEUP
    echo ${PRWDS}
    PRWDS2=$(rucio list-dids ${PRWDS}* --short | sort | grep -v tid | tail -n1)
    echo ${PRWDS2} >> ${OUTLIST}
done