#!/bin/bash

function runone_usage {
    cat <<EOF
Usage: ${0} [-d datadir] [-s sysname] [-m] [-w] [-l] [-f] mode config name filelist [filelist ...]" 1>&2;

-d path to output directory
-s systematic name (suffix after tree name)
-m input sample is Monte Carlo (--isMC option to xAH and normalize histograms after running)
-w use weighted branch when normalizing histograms (needs -m)
-l input files are paths (defualt is Rucio datasets)
-f input files are not lists
EOF
    exit 1; 
}

# extract options and their arguments into variables.
MC=0

EXTRA=""
TREESUFFIX=""
DATADIR="$(pwd)"

FOPTS="--inputRucio"
FLOPTS="--inputList"

NORMEXTRA=""

while getopts ":s:md:lfw" o ; do
    case "${o}" in
        s)
            TREESUFFIX="${OPTARG}"
            ;;
        m)
	    EXTRA="--isMC"
	    MC=1
            ;;
        w)
	    NORMEXTRA="-w"
            ;;
	d)
	    DATADIR="${OPTARG}"
	    ;;
	l)
	    FOPTS=""
	    ;;
	f)
	    FLOPTS=""
	    ;;
        *)
            runone_usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ "${#}" -lt 3 ]; then
    runone_usage
fi

# Defaults
MODE=${1}
CONFIG=${2}    
OUTNAME=${3}
FILELIST=${@:4}

# Mode
if [ "x${MODE}x" == "xcondorx" ]; then
    if [ ${OUTNAME} == "dijetjet_tla" ]; then
	RUNCODE=(condor --optFilesPerWorker 1 --optBatchWait --optCondorConf="+AccountingGroup = 'group_uct3.${USER}'")
    else
	RUNCODE=(condor --optFilesPerWorker 5 --optBatchWait --optCondorConf="+AccountingGroup = 'group_uct3.${USER}'")
    fi
elif [ "x${MODE}x" == "xslurmx" ]; then
    RUNCODE=(slurm --optBatchWait)
elif [ "x${MODE}x" == "xgridx" ]; then
    RUNCODE="prun --optGridDestSE=MWT2_UC_LOCALGROUPDISK  --optGridOutputSampleName=user.%nickname%.%in:name[1]%.%in:name[2]%.%in:name[3]%.20160222-test01/"
elif [ "x${MODE}x" == "xlsfx" ]; then
    RUNCODE=(lsf --optBatchWait --optBatchSharedFileSystem=1 --optResetShell=1 --optSubmitFlags="-q 8nh" --optBatchShellInit "export X509_USER_PROXY=$X509_USER_PROXY;")
else
    RUNCODE="direct"
fi

if [ -z ${PACKAGE} ]; then
    echo "Please set PACKAGE variable."
    exit -1
fi

# Hack for running on LBL
if [[ $(hostname) == pdsf* ]] || [[ $(hostname) == cori* ]] ; then
    FILELIST_list=$(cat $(echo ${FILELIST} | grep -o '[^ ]*\.list') | awk '{print "..\/filelists\/" $0 ".txt"}')
    FILELIST_txt=$(echo ${FILELIST} | grep -o '[^ ]*\.txt')
    FILELIST="${FILELIST_list} ${FILELIST_txt}"
    FLOPTS='--inputList'
    FOPTS=''
fi

xAH_run.py --files ${FILELIST} ${FOPTS} ${FLOPTS} -f --submitDir ${DATADIR}/OUT_${OUTNAME} --config ${AnalysisBase_PLATFORM}/data/${PACKAGE}/config_${CONFIG}.py --treeName outTree${TREESUFFIX} ${EXTRA} "${RUNCODE[@]}" || exit 1

echo "COMPLETED WITH ${?}"
if [ ${MC} == 1 ]; then
    # Rescale
    applyMiniTreeEventCountWeight.py ${NORMEXTRA} -e 36.21e3 -f '*mc16a*' ${DATADIR}/OUT_${OUTNAME} || exit 2
    applyMiniTreeEventCountWeight.py ${NORMEXTRA} -e 44.30e3 -f '*mc16d*' ${DATADIR}/OUT_${OUTNAME} || exit 2

    for fpath in $(find ${DATADIR}/OUT_${OUTNAME}/data-* -type f -name '*mc16a*'); do
    	applyTreeWeight -l 36.21e3 ${fpath} || exit 3
    done

    for fpath in $(find ${DATADIR}/OUT_${OUTNAME}/data-* -type f -name '*mc16d*'); do
    	applyTreeWeight -l 44.30e3 ${fpath} || exit 3
    done

    # Merge campaigns
    mergeCampaignHists.sh ${DATADIR}/OUT_${OUTNAME}
else
    for fpath in $(find ${DATADIR}/OUT_${OUTNAME}/data-* -type f); do
    	applyTreeWeight ${fpath} || exit 3
    done
fi
