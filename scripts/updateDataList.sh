#!/bin/bash

GRL=data16_13TeV.periodAllYear_DetStatus-v81-pro20-09_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS.xml
../scripts/makeDataList.py ../data/${GRL} | grep -v None > data16.AOD.list
../scripts/makeDataList.py ../data/${GRL} -d DAOD_EXOT10 | grep -v None > data16.EXOT10.list
../scripts/makeDataList.py ../data/${GRL} -d DAOD_EXOT6 | grep -v None > data16.EXOT6.list
../scripts/makeDataList.py ../data/${GRL} -d DAOD_EXOT2 | grep -v None > data16.EXOT2.list

../scripts/makeDataList.py -s debugrec_hlt -d DAOD_EXOT2 ../data/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml -y 15 | grep -v Missing > debug15.EXOT2.list
../scripts/makeDataList.py -s debugrec_hlt -d DAOD_EXOT6 ../data/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml -y 15 | grep -v Missing > debug15.EXOT6.list

../scripts/makeDataList.py -s debugrec_hlt -d DAOD_EXOT2 ../data/${GRL} | grep -v Missing > debug16.EXOT2.list
../scripts/makeDataList.py -s debugrec_hlt -d DAOD_EXOT6 ../data/${GRL} | grep -v Missing > debug16.EXOT6.list
