import ROOT
from xAODAnaHelpers import Config

c = Config()

GRL = "GoodRunsLists/data15_13TeV/data15_13TeV.periodAllYear_DetStatus-v71-pro19-06_DQDefects-00-01-02_PHYS_StandardGRL_All_Good_25ns.xml"

#
#  Data Config
#
if not args.is_MC:
    applyGRL           = True

#
#  MC Config
#
else:
    applyGRL           = False


#
# Process Ntuple
#
c.setalg("ProcessTrigStudy", { "m_name"                   : "TrigStudy",
                               "m_debug"                  : False, 
                               "m_detailLevel"            : 1,
                               "m_doCleaning"             : False,
                               "m_applyGRL"               : applyGRL,
                               "m_doData"                 : (not args.is_MC),
                               "m_GRLxml"                 : GRL,
                               } )

