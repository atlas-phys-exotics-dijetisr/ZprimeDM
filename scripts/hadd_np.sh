#!/bin/bash

if [ ${#} -ne 1 ]; then
    echo "usage: ${0} sampleDir"
    exit -1
fi

cd ${1}

function hadd_np {
    FILTER=${1}

    NDJ=$(find . -name "${FILTER}" | wc -l)
    if [ ${NDJ} != 0 ]; then
	for i in hist-${FILTER}
	do
	    RUN=$(echo ${i} | cut -d . -f 2)
	    NEWRUN=$(echo ${RUN} | cut -c 1-5)9
	    ALLRUN=$(echo ${RUN} | cut -c 1-5)*


 	    outname=${i/${RUN}./${NEWRUN}.}    
 	    outname=${outname/_dijet_Np1/}

	    inname=${i/${RUN}/${ALLRUN}}
 	    inname=${inname/Np1/Np*}
 	    inname=$(echo ${inname} | awk '{ for (i=NF; i>=1; i--) printf (i!=1) ? $i OFS : $i "\n" }')

 	    if [ -f ${outname} ]; then
 	     	rm ${outname}
 	    fi
	    hadd ${outname} ${inname}
	done
    fi
}

hadd_np 'MC15.999901.MGPy8EG_dmA_dijet_Np1_Jet100_*'
hadd_np 'MC15.999901.MGPy8EG_dmA_dijet_Np1_Jet300_*'
hadd_np 'MC15.999901.MGPy8EG_dmA_dijet_Np1_Jet350_*'
hadd_np 'MC15.999951.MGPy8EG_dmA_dijet_Np1_*'
hadd_np 'MC15.999871.MGPy8EG_dmA_dijet_Np1_*'
