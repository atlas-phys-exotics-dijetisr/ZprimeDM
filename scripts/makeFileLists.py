#!/usr/bin/python

import sys,os
import subprocess
import getpass

if len(sys.argv) < 3 :
  print "Requires at least two arguments!"
  print "1: tag to collect"
  print "2: username"
  exit(1)

tag = sys.argv[1]
username = sys.argv[2]
print "Collecting info on datasets with tag",tag,"for user",username

# Useful instructions for checking disk usage.
# To see what space is free:
# rucio list-rse-usage [myDataDisk]
# Check how much I have used up myself:
# rucio list-account-usage --rse [myDataDisk] [myUserName]
# Check what all the files you have on the disk are:
# rucio list-rules --account [myUserName] | grep [myDataDisk] | awk '{print $1,$3}'
# Delete them (will need to clean up eventually if we do this a lot!)
# rucio delete-rule [firstThingFromAboveCommand]
# Here is the r2d2 webpage if you need to request moving files by hand:
# https://rucio-ui.cern.ch/r2d2?account=qbuat&interval=1000d

# step 1: get a list of files
command = "rucio ls user."+username+".*{0}_tree.root/ --short | sort".format(tag)
filelist = subprocess.check_output(command,shell=True).splitlines()
filelist = [i.replace("user."+username+":","") for i in filelist if i]
filedict = {}

# step 2: check if they are all present on the local group disk of your choice,
# including that all files are present.
# make a list of those missing and print it out.
commandbase = "rucio list-dataset-replicas {0}"
for file in filelist :
  command = commandbase.format(file)
  output = subprocess.check_output(command,shell=True)
  content = output.split("DATASET")
  allPresent = True
  diskList = []
  for datasetInfo in content :
    datasetInfo = datasetInfo.split("\n")
    datasetInfo = [line for line in datasetInfo if (line and not "+" in line)]
    if not datasetInfo : continue
    dataset = datasetInfo[0].strip().strip(":")
    foundLongTermCopy = False
    onDisk = ""
    for line in datasetInfo[2:] :
      items = line.strip().split("|")
      items = [item.strip() for item in items if item]
      disk = items[0]
      found = eval(items[1])
      total = eval(items[2])
      if "LOCALGROUPDISK" in disk and found == total :
        foundLongTermCopy = True
        onDisk = disk
    if not foundLongTermCopy :
      allPresent = False
    else :
      if not onDisk in diskList: diskList.append(onDisk)
  filedict[file] = {"complete" : allPresent, "onDisks" : diskList}

# step 4: split filelist into separate components.
# this makes it possible to write a bunch of lists of files
separateLists = {}
for file in filedict.keys() :
  tokens = file.split(".")
  type = tokens[-5]
  channel = tokens[-6]
  samplename = tokens[-7]
  datasetName = tokens[-9]
  if "data" in type :
    if "data15" in datasetName :
      tag = "data15.{0}"
    elif "data16" in datasetName :
      tag = "data16.{0}"
    elif "data17" in datasetName :
      tag = "data17.{0}"
  elif "reco" in type:
    if "Pythia" in samplename :
      tag = "Pythia.{0}"
    elif "Sherpa" in samplename :
      tag = "Sherpa.{0}"
  elif "signal" in type :
    tag = "Signal.{0}"
  else :
    print "Unrecognized dataset type!"

  tag = tag.format(channel)

  remakefile = "{0}.NTUP.list".format(tag)
  #EC: don't think we're doing this anymore
  #if "signal" in type :
  #  remakefile = remakefile.replace("R21","R20p7")

  if not filedict[file]["complete"] :
    remakefile = "notOnLocalGroupDisks.NTUP.list"

  if not remakefile in separateLists.keys() :
    separateLists[remakefile] = []
  separateLists[remakefile].append(file)

# step 5: update relevant NTUP list in ZprimeDM/filelists
for remakefile in separateLists.keys() :
  filename = "ZprimeDM/filelists/"+remakefile
  outfile = open(filename,"w")
  for line in sorted(separateLists[remakefile]) :
    #throw out duplicate trees
    if "_dup_tree" in line:
      print "Warning: Discarding duplicate",line
      continue
    outfile.write(line+"\n")

  outfile.close()
  print "Created",filename

