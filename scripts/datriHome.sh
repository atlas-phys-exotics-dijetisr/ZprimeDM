#!/bin/bash

if [ ${#} != 2 ]; then
    echo "usage: ${0} LOCALGROUPDISK tag"
    exit -1
fi

DISK=${1}
TAG=${2}

rucio list-dids user.kkrizka:user.kkrizka.*.${TAG}_tree.root/
for i in $(rucio list-dids user.kkrizka:user.kkrizka.*.${TAG}_tree.root/ --short | sort)
do
    rucio add-rule ${i} 1 ${DISK} --comment fax --activity "User Subscriptions"
done
