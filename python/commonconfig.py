import ROOT
from xAODAnaHelpers import Config

import argparse
import shlex

import itertools

#
# List of GRLs
GRL={}
GRL['IGNORETOROID'] = ["GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml",
                       "GoodRunsLists/data16_13TeV/20170605/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS.xml",
                       "GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml",
                       "GoodRunsLists/data18_13TeV/20190318/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"]

GRL['HLT'] = ["GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml",
              "GoodRunsLists/data16_13TeV/20170605/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS.xml",
              "GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_JetHLT_Normal2017.xml",
              "GoodRunsLists/data18_13TeV/20190318/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"]

GRL['ALLGOOD'] = ["GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml",
                  "GoodRunsLists/data16_13TeV/20170605/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml",
                  "GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml",
                  "GoodRunsLists/data18_13TeV/20190318/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"]


LUMICALC={}
LUMICALC['IGNORETOROID'] = ["GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
                            "GoodRunsLists/data16_13TeV/20170605/PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS_297730-311481_OflLumi-13TeV-008.root",
                            "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root",
                            "GoodRunsLists/data18_13TeV/20190708/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root"]

LUMICALC['HLT'] = ["GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
                   "GoodRunsLists/data16_13TeV/20170605/PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS_297730-311481_OflLumi-13TeV-008.root",
                   "GoodRunsLists/data17_13TeV/20180619/physics_25ns_JetHLT_Normal2017.lumicalc.OflLumi-13TeV-010.root",
                   "GoodRunsLists/data18_13TeV/20190708/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root"]

LUMICALC['ALLGOOD'] = ["GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
                       "GoodRunsLists/data16_13TeV/20170605/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-008.root",
                       "GoodRunsLists/data17_13TeV/20190708/ilumicalc_histograms_None_325713-340453_OflLumi-13TeV-010.root",
                       "GoodRunsLists/data18_13TeV/20190708/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root"]


#
# b-tagging configurations
btagmodes=['FixedCutBEff']
btagWPs=[60,70,77,85]
btaggers=['DL1', 'DL1r'] #,'MV2r','MV2rmu','DL1r','DL1rmu']

def generate_btag_detailstr(btaggers=btaggers,btagmodes=btagmodes,btagWPs=btagWPs):
    detailStr=''
    for btagger,btagmode,btagWP in itertools.product(btaggers,btagmodes,btagWPs):
        detailStr+=' jetBTag_{btagger}_{btagmode}_{btagWP}'.format(btagger=btagger,btagWP=btagWP, btagmode=btagmode)
    return detailStr

class DoTrackJets(object):
    def __init__(self):
      raise TypeError(type(self).__name__ + ' is not intended for instances')
    TRUE = True
    FALSE = False
    FIXED_RADIUS_ONLY = 'FO'
    VARIABLE_RADIUS_ONLY = 'VO'


def apply_common_config(c,isMC=False,isAFII=False,mcCampaign=None,
                        triggerSelection='',
                        doSyst=None,
                        doJets=True, doFatJets=False, doTrackJets=DoTrackJets.FALSE, doPhotons=True, doMuons=False, doElectrons=False, doTruthFatJets = False, doHLTObjects=False,
                        jetBtaggers   =[],jetBtagmodes   =[],jetBtagWPs   =[],
                        fatJetBtaggers=[],fatJetBtagmodes=[],fatJetBtagWPs=[],
                        msgLevel=ROOT.MSG.INFO,GRLset='IGNORETOROID',
                        args=''):

    # Extra arguments
    
    parser = argparse.ArgumentParser(prog='apply_common_config')
    parser.add_argument("--doSyst"    , action='store_true', help='Enable systematics')
    parser.add_argument("--mcCampaign", dest='mcCampaign', help='Campaigns to enable for PRW')
    parser.add_argument("--doLooseNTUP", action='store_true', help='Set loose photon requirements, allowing purity studies')
    conargs=parser.parse_args(args=shlex.split(args))

    # Default is None for campaign and false for systs.
    
    doSyst = False
    if conargs.doSyst :
       print "Picking up from args:", conargs.doSyst
       doSyst = True
    else :
       print "Using default value for doSyst: false"
    if conargs.mcCampaign :
      mcCampaign=conargs.mcCampaign
    doLooseNTUP = conargs.doLooseNTUP

    #
    # Add sample metadata
    
    # SimulationFlavour
    # See the following for default tags
    # https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AtlasProductionGroupMC16a
    # https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AtlasProductionGroupMC16d
    #c.sample('*_a875_*',SimulationFlavour='AFII')
    #c.sample('*_s3126_*',SimulationFlavour='FS')

    #
    # Apply specific settings
    if not isMC: # Data Config
        jet_calibSeq  = 'JetArea_Residual_EtaJES_GSC_Insitu'
        systName      = ''
        systVal       = 0
        jetSystVector = ''
        fatjet_calibSeq = 'EtaJES_JMS_Insitu_InsituCombinedMass'
    else: # MC Config
        jet_calibSeq  = 'JetArea_Residual_EtaJES_GSC_Smear'
        systName      = "All" if doSyst else ''
        systVal       = 1 if doSyst else 0
        jetSystVector = "1.0,2.0,3.0" if doSyst else '' #"0.5,1,1.5,2,2.5,3" # dijet limit setting requirement
        fatjet_calibSeq = 'EtaJES_JMS'

    # AFII
    #
    if isAFII:
        JESUncertMCType    = "AFII"
        fatJESUncertMCType = "AFII"
    else:
        JESUncertMCType    = "MC16"
        fatJESUncertMCType = "MC16"


    
    GRLlist      = GRL     .get(GRLset, GRL     ['ALLGOOD'])
    LUMICALClist = LUMICALC.get(GRLset, LUMICALC['ALLGOOD'])

    # GoodRunsLists/data16_13TeV/20170215/
    BasicEventSelection = { "m_name"                  : "BasicEventSelection",
                            "m_msgLevel"              : msgLevel,
                            "m_applyGRLCut"           : True,
                            "m_GRLxml"                : ','.join(GRLlist),
                            "m_useMetaData"           : True,
                            "m_storeTrigDecisions"    : True,
                            "m_triggerSelection"      : triggerSelection,
                            "m_applyTriggerCut"       : not isMC,
                            "m_PVNTrack"              : 2,
                            "m_applyPrimaryVertexCut" : True,
                            "m_applyJetCleaningEventFlag" : True,
                            "m_applyEventCleaningCut" : True,
                            "m_applyCoreFlagsCut"     : True,
                            "m_doPUreweighting"       : isMC,
                            "m_autoconfigPRW"         : True,
                            "m_lumiCalcFileNames"     : ','.join(LUMICALClist),
                            "m_prwActualMu2017File"   : "GoodRunsLists/data17_13TeV/20190708/purw.actualMu.2017.root",
                            "m_prwActualMu2018File"   : "GoodRunsLists/data18_13TeV/20190708/purw.actualMu.2018.root",
                            "m_duplicatesStreamName"  : 'dup_tree',
                            "m_isMC"                  : isMC,
                            "m_setAFII"               : isAFII
                            }
    if mcCampaign!=None:
      BasicEventSelection["m_mcCampaign"] = mcCampaign

    c.algorithm("BasicEventSelection", BasicEventSelection )



    # Setup intermediate names, if needed
    doOR=doJets and doPhotons
    if doOR:
        nameJetContainer     ='Jets_Select'
        namePhotonContainer  ='Photons_Select'
        nameMuonContainer    ='Muons_Select'
        nameElectronContainer='Electrons_Select'
    else:
        nameJetContainer     ='SignalJets'
        namePhotonContainer  ='SignalPhotons'
        nameMuonContainer    ='SignalMuons'
        nameElectronContainer='SignalElectrons'

    if doJets:
        c.algorithm("JetCalibrator",      { "m_name"                    : "CalibrateJets",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "AntiKt4EMTopoJets",
                                            "m_outContainerName"        : "Jets_Calib",
                                            "m_outputAlgo"              : "Jets_Calib_Algo",
                                            "m_jetAlgo"                 : "AntiKt4EMTopo",
                                            "m_sort"                    : True,
                                            "m_saveAllCleanDecisions"   : True,
                                            "m_calibConfigAFII"         : "JES_MC16Recommendation_AFII_EMTopo_Apr2019_Rel21.config",
                                            "m_calibConfigFullSim"      : "JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config",
                                            "m_calibConfigData"         : "JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config",
                                            "m_calibSequence"           : jet_calibSeq,
                                            "m_setAFII"                 : isAFII,
                                            "m_uncertConfig"            : "rel21/Fall2018/R4_SR_Scenario1_SimpleJER.config",
                                            "m_uncertMCType"            : JESUncertMCType,
                                            "m_doCleaning"              : False,
                                            "m_redoJVT"                 : True,
                                            "m_systName"                : systName,
                                            "m_systValVectorString"     : jetSystVector,
                                            "m_systVal"                 : systVal,
                                            "m_writeSystToMetadata"     : True
                                            } )

        c.algorithm("JetSelector",        { "m_name"                    : "SelectJets",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "Jets_Calib",
                                            "m_inputAlgo"               : "Jets_Calib_Algo",
                                            "m_outContainerName"        : nameJetContainer,
                                            "m_outputAlgo"              : nameJetContainer+"_Algo",
                                            "m_decorateSelectedObjects" : True,
                                            "m_createSelectedContainer" : True,
                                            "m_cleanJets"               : False,
                                            "m_pT_min"                  : 25e3,
                                            "m_eta_max"                 : 2.8,
                                            "m_useCutFlow"              : True,
                                            "m_doBTagCut"               : False,
                                            "m_doJVF"                   : False,
                                            "m_doJVT"                   : True,
                                            "m_WorkingPointJVT"         : "Medium",
                                            "m_SFFileJVT"               : "JetJvtEfficiency/Moriond2018/JvtSFFile_EMTopoJets.root",
                                            "m_isMC"                    : isMC
                                            } )

        for btagger,btagmode,btagWP in itertools.product(jetBtaggers,jetBtagmodes,jetBtagWPs):
            btagWPstr="%s_%d"%(btagmode,btagWP)
            c.algorithm("BJetEfficiencyCorrector", { "m_name"                    : "BJetEffCor_"+nameJetContainer+"_"+btagger+"_"+btagWPstr,
                                                     "m_msgLevel"                : msgLevel,
                                                     "m_inContainerName"         : nameJetContainer,
                                                     "m_inputAlgo"               : nameJetContainer+"_Algo",
                                                     "m_systName"                : systName,
                                                     "m_systVal"                 : systVal,
                                                     "m_writeSystToMetadata"     : True,
                                                     "m_operatingPt"             : btagWPstr,
                                                     "m_corrFileName"            : "xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-10-19_v1.root",
                                                     "m_jetAuthor"               : "AntiKt4EMTopoJets",
                                                     "m_taggerName"              : btagger,
                                                     "m_EfficiencyCalibration"   : "auto",
                                                     "m_decor"                   : "BTag",
                                                     "m_isMC"                    : isMC
                                                     } )


    #
    # Fat jets
    if doFatJets:
        c.algorithm("JetCalibrator",      { "m_name"                    : "CalibrateFatJets",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets",
                                            "m_outContainerName"        : "FatJets_Calib",
                                            "m_outputAlgo"              : "FatJets_Calib_Algo",
                                            "m_jetAlgo"                 : "AntiKt10LCTopoTrimmedPtFrac5SmallR20",
                                            "m_sort"                    : True,
                                            "m_saveAllCleanDecisions"   : True,
                                            "m_calibConfigFullSim"      : "JES_MC16recommendation_FatJet_Trimmed_JMS_comb_17Oct2018.config",
                                            "m_calibConfigData"         : "JES_MC16recommendation_FatJet_Trimmed_JMS_comb_3April2019.config",
                                            "m_calibSequence"           : fatjet_calibSeq,
                                            "m_forceInsitu"             : False,
                                            "m_setAFII"                 : isAFII,
                                            "m_uncertConfig"            : "rel21/Spring2019/R10_CategoryReduction.config",
                                            "m_uncertMCType"            : fatJESUncertMCType,
                                            "m_doCleaning"              : False,
                                            "m_applyFatJetPreSel"       : True,    # make sure fat-jet uncertainty is applied only in valid region
                                            "m_systName"                : systName,
                                            "m_systVal"                 : systVal,
                                            "m_systValVectorString"     : jetSystVector,
                                            "m_writeSystToMetadata"     : True
                                            } )

        c.algorithm("JetSelector",        { "m_name"                    : "SelectFatJets",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "FatJets_Calib",
                                            "m_inputAlgo"               : "FatJets_Calib_Algo",
                                            "m_outContainerName"        : "SignalFatJets",
                                            "m_outputAlgo"              : "SignalFatJets_Algo",
                                            "m_decorateSelectedObjects" : True,
                                            "m_createSelectedContainer" : True,
                                            "m_cleanJets"               : False,
                                            "m_pT_min"                  : 200e3,
                                            "m_eta_max"                 : 2.0,
                                            "m_useCutFlow"              : True
                                            } )
    #
    # Track Jets
    if doTrackJets:
        if not doTrackJets == DoTrackJets.VARIABLE_RADIUS_ONLY:
            c.algorithm("JetSelector", { "m_name"                    : "SelectFixedRTrackJets",
                                         "m_inContainerName"         : "AntiKt2PV0TrackJets",
                                         "m_outContainerName"        : "SignalFixedRTrackJets",
                                         "m_decorateSelectedObjects" : False,
                                         "m_createSelectedContainer" : True,
                                         "m_cleanJets"               : True,
                                         "m_pT_min"                  : 10e3,
                                         "m_eta_max"                 : 2.5,
                                         "m_useCutFlow"              : True,
                                         "m_doJVF"                   : False
                                         } )

        if not doTrackJets == DoTrackJets.FIXED_RADIUS_ONLY:
            c.algorithm("JetSelector", { "m_name"                    : "SelectVRTrackJets",
                                         "m_inContainerName"         : "AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903",
                                         "m_outContainerName"        : "SignalVRTrackJets",
                                         "m_decorateSelectedObjects" : False,
                                         "m_createSelectedContainer" : True,
                                         "m_cleanJets"               : True,
                                         "m_pT_min"                  : 10e3,
                                         "m_eta_max"                 : 2.5,
                                         "m_useCutFlow"              : True,
                                         "m_doJVF"                   : False
                                         } )
        """ # these dont have an entry in the btagging calib file yet
            c.algorithm("JetSelector", { "m_name"                    : "SelectVRTrackGhostTagJets",
                                         "m_inContainerName"         : "AntiKtVR30Rmax4Rmin02TrackGhostTagJets",
                                         "m_outContainerName"        : "SignalVRTrackGhostTagJets",
                                         "m_decorateSelectedObjects" : False,
                                         "m_createSelectedContainer" : True,
                                         "m_cleanJets"               : True,
                                         "m_pT_min"                  : 10e3,
                                         "m_eta_max"                 : 2.5,
                                         "m_useCutFlow"              : True,
                                         "m_doJVF"                   : False
                                         } )
        """
        for btagger,btagmode,btagWP in itertools.product(fatJetBtaggers,fatJetBtagmodes,fatJetBtagWPs):
            btagWPstr="%s_%d"%(btagmode,btagWP)
            if not doTrackJets == DoTrackJets.VARIABLE_RADIUS_ONLY:
                c.algorithm("BJetEfficiencyCorrector", { "m_name"                    : "BJetEffCor_SignalFixedRTrackJets_"+btagger+"_"+btagWPstr,
                                                         "m_msgLevel"                : msgLevel,
                                                         "m_inContainerName"         : "SignalFixedRTrackJets",
                                                         "m_systName"                : systName,
                                                         "m_systVal"                 : systVal,
                                                         "m_operatingPt"             : btagWPstr,
                                                         "m_corrFileName"            : "xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-03-11_v2.root",
                                                         "m_jetAuthor"               : "AntiKt2PV0TrackJets",
                                                         "m_taggerName"              : btagger,
                                                         "m_EfficiencyCalibration"   : "auto",
                                                         "m_decor"                   : "BTag",
                                                         "m_outputSystName"          : "BTag_FixedRJets"
                                                         } )

            if not doTrackJets == DoTrackJets.FIXED_RADIUS_ONLY:
                c.algorithm("BJetEfficiencyCorrector", { "m_name"                    : "BJetEffCor_SignalVRTrackJets_"+btagger+"_"+btagWPstr,
                                                         "m_msgLevel"                : msgLevel,
                                                         "m_inContainerName"         : "SignalVRTrackJets",
                                                         "m_systName"                : systName,
                                                         "m_systVal"                 : systVal,
                                                         "m_operatingPt"             : btagWPstr,
                                                         "m_corrFileName"            : "xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-03-11_v2.root",
                                                         "m_jetAuthor"               : "AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903",
                                                         "m_taggerName"              : btagger,
                                                         "m_EfficiencyCalibration"   : "auto",
                                                         "m_decor"                   : "BTag",
                                                         "m_outputSystName"          : "BTag_VRJets"
                                                         } )
            """ # these dont have an entry in the btagging calib file yet
            if not doTrackJets == DoTrackJets.FIXED_RADIUS_ONLY:
                c.algorithm("BJetEfficiencyCorrector", { "m_name"                    : "BJetEffCor_SignalVRTrackGhostTagJets_"+btagger+"_"+btagWPstr,
                                                         "m_msgLevel"                : msgLevel,
                                                         "m_inContainerName"         : "SignalVRTrackGhostTagJets",
                                                         "m_systName"                : systName,
                                                         "m_systVal"                 : systVal,
                                                         "m_operatingPt"             : btagWPstr,
                                                         "m_corrFileName"            : "xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-03-11_v2.root",
                                                         "m_jetAuthor"               : "AntiKtVR30Rmax4Rmin02TrackGhostTagJets",
                                                         "m_taggerName"              : btagger,
                                                         "m_EfficiencyCalibration"   : "auto",
                                                         "m_decor"                   : "BTag",
                                                         "m_outputSystName"          : "BTag_VRJetsGhostTag"
                                                         } )

            """
    #
    # Photons
    if doPhotons:
        c.algorithm("PhotonCalibrator",   { "m_name"                    : "CalibratePhotons",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "Photons",
                                            "m_outContainerName"        : "Photons_Calib",
                                            "m_outputAlgoSystNames"     : "Photons_Calib_Algo",
                                            #"m_photonCalibMap"          : "PhotonEfficiencyCorrection/2015_2017/rel21.2/Winter2018_Prerec_v1/map0.txt", # Updated March 18 2018
                                            "m_tightIDConfigPath"       : "ElectronPhotonSelectorTools/offline/20180116/PhotonIsEMTightSelectorCutDefs.conf", # Updated March 18 2018
#                                            "m_mediumIDConfigPath"      : "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMMediumSelectorCutDefs.conf", # Removed March 18 2018
                                            "m_looseIDConfigPath"       : "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMLooseSelectorCutDefs.conf", # Checked March 15 2018
                                            "m_esModel"                 : "es2018_R21_v0", # Updated September 30 2019
                                            "m_decorrelationModel"      : "1NP_v1",        # Checked March 15 2018
                                            "m_useAFII"                 : isAFII,
                                            "m_systName"                : systName,
                                            "m_systVal"                 : systVal,
                                            "m_sort"                    : True,
                                            "m_isMC"                    : isMC,
                                            "m_readIDFlagsFromDerivation": True, #added by EEC 050718 so that new AODs with reduced egamma vars can be read
                                            } )

        isoVar = "m_MinIsoWPCut" if not doLooseNTUP else "m_IsoWPList"
        isoVal = "FixedCutTightCaloOnly" if not doLooseNTUP else "FixedCutTightCaloOnly,FixedCutTight,FixedCutLoose"

        c.algorithm("PhotonSelector",     { "m_name"                    : "SelectPhotons",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "Photons_Calib",
                                            "m_inputAlgoSystNames"      : "Photons_Calib_Algo",
                                            "m_outContainerName"        : namePhotonContainer,
                                            "m_outputAlgoSystNames"     : namePhotonContainer+"_Algo",
                                            "m_decorateSelectedObjects" : True,
                                            "m_createSelectedContainer" : True,
                                            "m_pT_min"                  : 10e3,
                                            "m_eta_max"                 : 2.37,
                                            "m_vetoCrack"               : True,
                                            "m_doAuthorCut"             : True,
                                            "m_doOQCut"                 : True,
                                            "m_systName"                : systName,
                                            "m_photonIdCut"             : "Tight" if not doLooseNTUP else "Loose",
                                            isoVar                      : isoVal,
                                            "m_isMC"                    : isMC,
                                            "m_readOQFromDerivation"    : True,  #added by EEC 050718 so that new AODs with reduced egamma vars can be read
                                            } )

    #
    # Muons
    if doMuons:
        c.algorithm("MuonCalibrator", { "m_name"                   : "CalibrateMuons",
                                        "m_msgLevel"               : msgLevel,
                                        "m_inContainerName"        : "Muons",
                                        "m_outContainerName"       : "Muons_Calib",
                                        "m_outputAlgoSystNames"    : "Muons_Calib_Algo"
                                        } )

        c.algorithm("MuonSelector", { "m_name"                    : "SelectMuons", 
                                      "m_msgLevel"                : msgLevel,
                                      "m_inContainerName"         : "Muons_Calib", 
                                      "m_outContainerName"        : nameMuonContainer, 
                                      "m_createSelectedContainer" : True,
                                      "m_pT_min"                  : 10e3,
                                      "m_eta_max"                 : 2.5,
                                      "m_muonQualityStr"          : "Medium",
                                      "m_d0sig_max"               : 3,
                                      "m_z0sintheta_max"          : 1.0,
                                      "m_MinIsoWPCut"             : "Loose"
                                      } )

    #
    # Electrons
    if doElectrons:
        c.algorithm("ElectronCalibrator", { "m_name"                : "CalibrateElectrons",
                                            "m_msgLevel"            : msgLevel,
                                            "m_inContainerName"     : "Electrons",
                                            "m_outContainerName"    : "Electrons_Calib",
                                            "m_outputAlgoSystNames" : "Electrons_Calib_Algo",
                                            "m_esModel"             : "es2018_R21_v0",
                                            #"m_decorrelationModel"  : "FULL_v1"
                                            "m_decorrelationModel"  : "1NP_v1"
                                            })

        c.algorithm("ElectronSelector", { "m_name"                    : "SelectElectrons",
                                          "m_inContainerName"         : "Electrons_Calib",
                                          "m_inputAlgoSystNames"      : "Electrons_Calib_Algo",
                                          "m_outContainerName"        : nameElectronContainer,
                                          "m_outputAlgoSystNames"     : nameElectronContainer+"_Algo",
                                          "m_createSelectedContainer" : True,
                                          "m_doLHPID"                 : True,
                                          "m_doLHPIDcut"              : True,
                                          "m_LHOperatingPoint"        : "Loose",
                                          "m_MinIsoWPCut"             : "LooseTrackOnly",
                                          "m_d0sig_max"               : 5.,
                                          "m_z0sintheta_max"          : 0.5,
                                          "m_pT_min"                  : 7*1000.,
                                          "m_eta_max"                 : 2.47
                                          })

    if doOR:
        OverlapRemover={ "m_name"                    : "RemoveOverlaps",
                         "m_outputAlgoSystNames"     : "ORAlgo_Syst",
                         "m_createSelectedContainers": True}
        if doJets:
            OverlapRemover["m_inContainerName_Jets"]      =nameJetContainer
            OverlapRemover["m_inputAlgoJets"]             =nameJetContainer+"_Algo"
            OverlapRemover["m_outContainerName_Jets"]     ="SignalJets"
        if doPhotons:
            OverlapRemover["m_inContainerName_Photons"]   =namePhotonContainer
            OverlapRemover["m_inputAlgoPhotons"]          =namePhotonContainer+"_Algo"
            OverlapRemover["m_outContainerName_Photons"]  ="SignalPhotons"
        if doMuons:
            OverlapRemover["m_inContainerName_Muons"]     =nameMuonContainer
            OverlapRemover["m_inputAlgoMuons"]            =nameMuonContainer+"_Algo"
            OverlapRemover["m_outContainerName_Muons"]    ="SignalMuons"
        if doElectrons:
            OverlapRemover["m_inContainerName_Electrons"] =nameElectronContainer
            OverlapRemover["m_inputAlgoElectrons"]        =nameElectronContainer+"_Algo"
            OverlapRemover["m_outContainerName_Electrons"]="SignalElectrons"

        c.algorithm("OverlapRemover", OverlapRemover)

    if doHLTObjects:
        c.algorithm("JetSelector",        { "m_name"                    : "SelectHLTJets",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "HLT_xAOD__JetContainer_a4tcemsubjesISFS",
                                            "m_outContainerName"        : "SelectedHLTJets",
                                            "m_outputAlgo"              : "SelectedHLTJets_Algo",
                                            "m_decorateSelectedObjects" : True,
                                            "m_createSelectedContainer" : True,
                                            "m_cleanJets"               : False,
                                            "m_pT_min"                  : 20e3,
                                            "m_eta_max"                 : 3.0,
                                            "m_useCutFlow"              : False,
                                            "m_doBTagCut"               : False,
                                            "m_doJVF"                   : False,
                                            "m_doJVT"                   : False,
                                            "m_isMC"                    : isMC
                                            } )



    if doTruthFatJets:
        c.algorithm("JetSelector",        { "m_name"                    : "SelectFatTruthJets",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "AntiKt10TruthTrimmedPtFrac5SmallR20Jets",
                                            "m_outContainerName"        : "SelectedTruthFatJets",
                                            "m_outputAlgo"              : "SelectedTruthFatJets_Algo",
                                            "m_decorateSelectedObjects" : True,
                                            "m_createSelectedContainer" : True,
                                            "m_cleanJets"               : False,
                                            "m_pT_min"                  : 150e3,
                                            "m_eta_max"                 : 2.5,
                                            "m_useCutFlow"              : False,
                                            "m_doBTagCut"               : False,
                                            "m_doJVF"                   : False,
                                            "m_doJVT"                   : False,
                                            "m_isMC"                    : isMC
                                            } )

def findAlgo(c,name):
    for algo in c._algorithms:
        if algo.m_name==name: return algo
    return None
