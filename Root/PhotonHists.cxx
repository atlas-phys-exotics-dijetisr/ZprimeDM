#include <ZprimeDM/PhotonHists.h>

using namespace ZprimeDM;

PhotonHists :: PhotonHists (const std::string& name, const std::string& detailStr, const std::string& prefix)
  : HistogramManager(name, detailStr), m_debug(false), m_infoSwitch(detailStr), m_prefix(prefix)
{ }

PhotonHists :: ~PhotonHists () 
{ }

StatusCode PhotonHists::initialize()
{

  // kinematic
  if(m_infoSwitch.m_kinematic)
    {
      h_pt   = book(m_name, "pt"  , m_prefix+" photon p_{T} [GeV]", 100, 0, 500);
      h_pt_m = book(m_name, "pt_m", m_prefix+" photon p_{T} [GeV]", 100, 0, 1000);
      h_pt_l = book(m_name, "pt_l", m_prefix+" photon p_{T} [GeV]", 100, 0, 5000);

      //static const double eta_var_bins[] = {-3.0, -2.9, -2.8, -2.7, -2.6, -2.5, -2.4, -2.3, -2.2, -2.1, -2.0, -1.9, -1.8, -1.7, -1.6, -1.52, -1.37, -1.3, -1.2, -1.1, -1.0, -0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1, 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.37, 1.52, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0};
      //static int binnum = sizeof(eta_var_bins)/sizeof(double) - 1; 
      //h_eta  = book(m_name, "eta" , m_prefix+" photon #eta", binnum, eta_var_bins );
      h_eta  = book(m_name, "eta" , m_prefix+" photon #eta", 60, -3          , 3);
      h_phi  = book(m_name, "phi" , m_prefix+" photon #phi", 50, -TMath::Pi(), TMath::Pi());
    }

  // isolation
  if(m_infoSwitch.m_isolation)
    {
      h_isIsolated_FixedCutTightCaloOnly = book(m_name, "isIsolated_FixedCutTightCaloOnly" , m_prefix+" photon FixedCutTightCaloOnly Passed", 2, -0.5 , 1.5);
      h_isIsolated_FixedCutTight         = book(m_name, "isIsolated_FixedCutTight"         , m_prefix+" photon FixedCutTight Passed"        , 2, -0.5 , 1.5);
      h_isIsolated_FixedCutLoose         = book(m_name, "isIsolated_FixedCutLoose"         , m_prefix+" photon FixedCutLoose Passed"        , 2, -0.5 , 1.5);

      h_ptcone20     = book(m_name, "ptcone20",     m_prefix+" photon ptcone20 [GeV]"    , 100, -10, 40);
      h_ptcone30     = book(m_name, "ptcone30",     m_prefix+" photon ptcone30 [GeV]"    , 100, -10, 40);
      h_ptcone40     = book(m_name, "ptcone40",     m_prefix+" photon ptcone40 [GeV]"    , 100, -10, 40);

      h_ptvarcone20  = book(m_name, "ptvarcone20",  m_prefix+" photon ptvarcone20 [GeV]" , 100, -10, 40);
      h_ptvarcone30  = book(m_name, "ptvarcone30",  m_prefix+" photon ptvarcone30 [GeV]" , 100, -10, 40);
      h_ptvarcone40  = book(m_name, "ptvarcone40",  m_prefix+" photon ptvarcone40 [GeV]" , 100, -10, 40);

      h_topoetcone20 = book(m_name, "topoetcone20", m_prefix+" photon topoetcone20 [GeV]", 50, -10, 40);
      h_topoetcone30 = book(m_name, "topoetcone30", m_prefix+" photon topoetcone30 [GeV]", 50, -10, 40);
      h_topoetcone40 = book(m_name, "topoetcone40", m_prefix+" photon topoetcone40 [GeV]", 50, -10, 40);
    }

  // PID
  if(m_infoSwitch.m_PID)
    {
      h_PhotonID_Loose  = book(m_name, "PhotonID_Loose",  m_prefix+" photon is loose",  2, -0.5, 1.5);
      h_PhotonID_Medium = book(m_name, "PhotonID_Medium", m_prefix+" photon is medium", 2, -0.5, 1.5);
      h_PhotonID_Tight  = book(m_name, "PhotonID_Tight",  m_prefix+" photon is tight",  2, -0.5, 1.5);
    }
  
  // purity
  
  if(m_infoSwitch.m_purity)
    {
      h_radhad1 = book(m_name, "radhad1" , m_prefix+" photon radhad1", 100, -0.5 , 0.5);
      h_radhad = book(m_name, "radhad", m_prefix+" photon radhad", 100, -1.0, 1.0);
      h_e277 = book(m_name, "e277", m_prefix+" photon e277", 100, 0, 2.5e10);
      h_reta = book(m_name, "reta", m_prefix+" photon reta", 50, 0, 12);
      h_rphi = book(m_name, "rphi", m_prefix+" photon rphi", 100, -2.0, 2.0);
      h_weta2 = book(m_name, "weta2", m_prefix+" photon weta2", 50, -0.025, 0.025);
      h_f1 = book(m_name, "f1", m_prefix+" photon f1", 100, 0, 1);
      h_wtot = book(m_name, "wtot", m_prefix+" photon wtot", 100, -10, 10);
      h_deltae = book(m_name, "deltae", m_prefix+" photon deltae", 100, 0, 500);
      h_eratio = book(m_name, "eratio", m_prefix+" photon eratio", 100, 0, 1);
    }
  

  return StatusCode::SUCCESS;
}

StatusCode PhotonHists::execute(const xAH::Photon* photon, float eventWeight)
{
  ANA_CHECK(HistogramManager::execute());

  if(m_infoSwitch.m_kinematic)
    {
      h_pt  ->Fill(photon->p4.Pt(),eventWeight);
      h_pt_m->Fill(photon->p4.Pt(),eventWeight);
      h_pt_l->Fill(photon->p4.Pt(),eventWeight);

      h_eta->Fill(photon->p4.Eta(),eventWeight);
      h_phi->Fill(photon->p4.Phi(),eventWeight);
    }

  if(m_infoSwitch.m_isolation)
    {
      h_isIsolated_FixedCutTightCaloOnly->Fill(photon->isIsolated_Cone40CaloOnly,eventWeight);
      h_isIsolated_FixedCutTight        ->Fill(photon->isIsolated_Cone40        ,eventWeight);
      h_isIsolated_FixedCutLoose        ->Fill(photon->isIsolated_Cone20        ,eventWeight);

      h_ptcone20    ->Fill(photon->ptcone20    ,eventWeight);
      h_ptcone30    ->Fill(photon->ptcone30    ,eventWeight);
      h_ptcone40    ->Fill(photon->ptcone40    ,eventWeight);

      h_ptvarcone20 ->Fill(photon->ptvarcone20 ,eventWeight);
      h_ptvarcone30 ->Fill(photon->ptvarcone30 ,eventWeight);
      h_ptvarcone40 ->Fill(photon->ptvarcone40 ,eventWeight);

      h_topoetcone20->Fill(photon->topoetcone20,eventWeight);
      h_topoetcone30->Fill(photon->topoetcone30,eventWeight);
      h_topoetcone40->Fill(photon->topoetcone40,eventWeight);
    }

  if(m_infoSwitch.m_PID)
    {
      h_PhotonID_Loose ->Fill(photon->IsLoose ,eventWeight);
      h_PhotonID_Medium->Fill(photon->IsMedium,eventWeight);
      h_PhotonID_Tight ->Fill(photon->IsTight ,eventWeight);
    }
  
  if(m_infoSwitch.m_purity)
    {
      h_radhad1->Fill(photon->radhad1,eventWeight);
      h_radhad->Fill(photon->radhad,eventWeight);
      h_e277->Fill(photon->e277,eventWeight);
      h_reta->Fill(photon->reta,eventWeight);
      h_rphi->Fill(photon->rphi,eventWeight);
      h_weta2->Fill(photon->weta2,eventWeight);
      h_f1->Fill(photon->f1,eventWeight);
      h_wtot->Fill(photon->wtot,eventWeight);
      h_deltae->Fill(photon->deltae,eventWeight);
      h_eratio->Fill(photon->eratio,eventWeight);
    
    }

  return StatusCode::SUCCESS;
}
