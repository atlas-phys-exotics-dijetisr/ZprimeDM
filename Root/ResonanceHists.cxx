#include <ZprimeDM/ResonanceHists.h>

ResonanceHists :: ResonanceHists (const std::string& name, const std::string& detailStr) 
  : HistogramManager(name, detailStr)
{}

ResonanceHists :: ~ResonanceHists () 
{}

StatusCode ResonanceHists::initialize()
{
  std::cout << "ResonanceHists::initialize()" << std::endl;

  // These plots are always made
  h_pt     = book(m_name, "pt",      "p_{T,R} [GeV]", 100,            0,     500   );
  h_pt_l   = book(m_name, "pt_l",    "p_{T,R} [GeV]", 100,            0,    1000   );
  h_eta    = book(m_name, "eta",     "#eta_{R}",      100,           -4,       4   );
  h_phi    = book(m_name, "phi",     "#phi_{R}",      100, -TMath::Pi(),TMath::Pi());
  h_m      = book(m_name, "m",       "m_{R} [GeV]",  2500,            0,    5000   );

  return StatusCode::SUCCESS;
}

StatusCode ResonanceHists::execute(const xAH::Particle* reso0, const xAH::Particle* reso1, float eventWeight) 
{
  TLorentzVector p4_Zprime=reso0->p4+reso1->p4;

  h_pt    ->Fill(p4_Zprime.Pt() , eventWeight);
  h_pt_l  ->Fill(p4_Zprime.Pt() , eventWeight);
  h_eta   ->Fill(p4_Zprime.Eta(), eventWeight);
  h_phi   ->Fill(p4_Zprime.Phi(), eventWeight);
  h_m     ->Fill(p4_Zprime.M()  , eventWeight);

  return StatusCode::SUCCESS;
}
