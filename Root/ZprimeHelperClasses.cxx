#include "ZprimeDM/ZprimeHelperClasses.h"

namespace ZprimeHelperClasses
{

  void DijetISRInfoSwitch::initialize()
  {
    m_mass = has_exact("mass");
    m_kinematic = has_exact("kinematic");
    m_deltas = has_exact("deltas");
    m_rapidities = has_exact("rapidities");
    m_asymmetries = has_exact("asymmetries");
    m_softerjets = has_exact("softerjets");
    m_truthz = has_exact("truthz");
    m_2d     = has_exact("2d");
    m_debug  = has_exact("debug");
  }
} // close namespace HelperClasses
