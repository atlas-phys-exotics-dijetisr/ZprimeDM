#include <ZprimeDM/ZprimeTruthResonanceHists.h>

ZprimeTruthResonanceHists :: ZprimeTruthResonanceHists (const std::string& name, const std::string& detailStr) 
  : HistogramManager(name, detailStr)
{}

ZprimeTruthResonanceHists :: ~ZprimeTruthResonanceHists () 
{}

StatusCode ZprimeTruthResonanceHists::initialize()
{
  std::cout << "ZprimeTruthResonanceHists::initialize()" << std::endl;

  // These plots are always made
  h_zpt   = book(m_name, "zpt",   "p_{T,Z'} [GeV]", 100,            0,     500   );
  h_zeta  = book(m_name, "zeta",  "#eta_{Z'}",      100,           -4,       4   );
  h_zphi  = book(m_name, "zphi",  "#phi_{Z'}",      100, -TMath::Pi(),TMath::Pi());
  h_zm    = book(m_name, "zm",    "m_{Z'} [GeV]",   500,            0,    5000   );

  return StatusCode::SUCCESS;
}

StatusCode ZprimeTruthResonanceHists::execute(const TLorentzVector& Zprime, float eventWeight) 
{
  h_zpt ->Fill(Zprime.Pt()     , eventWeight);
  if(Zprime.Pt()>0)
    {
      h_zeta->Fill(Zprime.Eta()    , eventWeight);
      h_zphi->Fill(Zprime.Phi()    , eventWeight);
    }
  else
    {
      h_zeta->Fill(0. , eventWeight);
      h_zphi->Fill(0. , eventWeight);
    }
  h_zm  ->Fill(Zprime.M()      , eventWeight);

  return StatusCode::SUCCESS;
}
