#include <ZprimeDM/MuonHists.h>

using namespace ZprimeDM;

MuonHists :: MuonHists (const std::string& name, const std::string& detailStr, const std::string& prefix)
  : HistogramManager(name, detailStr), m_debug(false), m_infoSwitch(detailStr), m_prefix(prefix)
{ }

MuonHists :: ~MuonHists () 
{ }

StatusCode MuonHists::initialize()
{

  // kinematic
  if(m_infoSwitch.m_kinematic)
    {
      h_pt   = book(m_name, "pt"  , m_prefix+" muon p_{T} [GeV]", 100, 0, 500);
      h_pt_m = book(m_name, "pt_m", m_prefix+" muon p_{T} [GeV]", 100, 0, 1000);
      h_pt_l = book(m_name, "pt_l", m_prefix+" muon p_{T} [GeV]", 100, 0, 5000);

      //static const double eta_var_bins[] = {-3.0, -2.9, -2.8, -2.7, -2.6, -2.5, -2.4, -2.3, -2.2, -2.1, -2.0, -1.9, -1.8, -1.7, -1.6, -1.52, -1.37, -1.3, -1.2, -1.1, -1.0, -0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1, 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.37, 1.52, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0};
      //static int binnum = sizeof(eta_var_bins)/sizeof(double) - 1; 
      //h_eta  = book(m_name, "eta" , m_prefix+" muon #eta", binnum, eta_var_bins );
      h_eta  = book(m_name, "eta" , m_prefix+" muon #eta", 60, -3          , 3);
      h_phi  = book(m_name, "phi" , m_prefix+" muon #phi", 50, -TMath::Pi(), TMath::Pi());
    }

  return StatusCode::SUCCESS;
}

StatusCode MuonHists::execute(const xAH::Muon* muon, float eventWeight)
{
  ANA_CHECK(HistogramManager::execute());

  if(m_infoSwitch.m_kinematic)
    {
      h_pt  ->Fill(muon->p4.Pt(),eventWeight);
      h_pt_m->Fill(muon->p4.Pt(),eventWeight);
      h_pt_l->Fill(muon->p4.Pt(),eventWeight);

      h_eta->Fill(muon->p4.Eta(),eventWeight);
      h_phi->Fill(muon->p4.Phi(),eventWeight);
    }

  return StatusCode::SUCCESS;
}
