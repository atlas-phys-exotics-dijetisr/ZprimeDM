#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

#include <AsgTools/MessageCheck.h>

#include <ZprimeDM/ZprimeJetHistsAlgo.h>
#include <xAODAnaHelpers/HelperFunctions.h>

#include "TFile.h"
#include "TKey.h"
#include "TLorentzVector.h"
#include "TSystem.h"

#include <utility>      
#include <iostream>
#include <fstream>

using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(ZprimeJetHistsAlgo)

ZprimeJetHistsAlgo :: ZprimeJetHistsAlgo () :
  m_mc(false),
  m_jetDetailStr(""),
  m_doPUReweight(false),
  m_doCleaning(false),
  m_jetPtCleaningCut(25.),
  m_doTrigger(false),
  m_dumpTrig(false),
  m_trigger(""),
  h_jet0(nullptr)
{
  ANA_MSG_INFO("ZprimeJetHistsAlgo::ZprimeJetHistsAlgo()");
}

EL::StatusCode ZprimeJetHistsAlgo :: histInitialize ()
{
  ANA_CHECK(xAH::Algorithm::algInitialize());
  ANA_MSG_INFO("ZprimeJetHistsAlgo::histInitialize()");

  //
  // data model
  m_event=DijetISREvent::global();

  //
  // Cutflow
  m_cutflow=new CutflowHists(m_name, "");
  ANA_CHECK(m_cutflow->initialize());

  m_cf_trigger =m_cutflow->addCut("trigger");
  m_cf_cleaning=m_cutflow->addCut("cleaning");
  m_cf_jet     =m_cutflow->addCut("jet");
  m_cutflow->record(wk());

  //
  // Histograms
  h_jet0      =new ZprimeDM::JetHists(m_name+"/jet0/", m_jetDetailStr);
  ANA_CHECK(h_jet0->initialize());
  h_jet0->record(wk());

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZprimeJetHistsAlgo :: initialize ()
{
  ANA_MSG_DEBUG("ZprimeJetHistsAlgo::initialize()");

  // Trigger
  std::string token;
  std::istringstream ss(m_trigger);

  while(std::getline(ss, token, ','))
    m_triggers.push_back(token);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZprimeJetHistsAlgo :: execute ()
{
  ANA_MSG_DEBUG("ZprimeJetHistsAlgo::execute()");

  //
  // Cuts
  float eventWeight    =m_event->m_weight;
  if(m_mc && m_doPUReweight)
    eventWeight *= m_event->m_weight_pileup;

  //
  // do trigger
  if(m_doTrigger)
    {
      ANA_MSG_DEBUG("Doing Trigger");

      if(m_dumpTrig)
	{
	  for(std::string& thisTrig:*m_event->m_passedTriggers)
	    ANA_MSG_DEBUG("\t" << thisTrig);
	}

      //
      // trigger
      bool passTrigger=true;
      for(const std::string& trigger : m_triggers)
	passTrigger &= (std::find(m_event->m_passedTriggers->begin(), m_event->m_passedTriggers->end(), trigger ) != m_event->m_passedTriggers->end());

      if(!passTrigger)
	{
	  ANA_MSG_DEBUG(" Fail Trigger");
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_trigger, eventWeight);
    }

  //
  // doing cleaning
  //
  bool  passCleaning   = true;
  if(!m_event->m_doTruthOnly)
    {
      for(unsigned int i = 0;  i<m_event->jets(); ++i)
	{
	  const xAH::Jet* jet=m_event->jet(i);
	  if(jet->p4.Pt() > m_jetPtCleaningCut)
	    {
	      if(!m_doCleaning && !jet->clean_passLooseBad)
		{
		  ANA_MSG_DEBUG(" Skipping jet clean");
		  continue;
		}
	      if(!jet->clean_passLooseBad) passCleaning = false;
	    }
	  else
	    break;
	}
    }

  //
  //  Jet Cleaning 
  //
  if(m_doCleaning && !passCleaning)
    {
      ANA_MSG_DEBUG(" Fail cleaning");
      return EL::StatusCode::SUCCESS;
    }
  m_cutflow->execute(m_cf_cleaning, eventWeight);

  //
  // At least one jet
  //
  if(m_event->jets()==0)
    {
      ANA_MSG_DEBUG(" Fail nJets>0");
      return EL::StatusCode::SUCCESS;
    }
  m_cutflow->execute(m_cf_jet, eventWeight);

  ANA_MSG_DEBUG(" Pass All Cuts");

  //
  //Filling
  h_jet0->execute(m_event->jet(0), eventWeight);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZprimeJetHistsAlgo :: histFinalize ()
{
  ANA_CHECK(m_cutflow->finalize());
  delete m_cutflow;

  ANA_CHECK(h_jet0->finalize());
  delete h_jet0;
  
  return EL::StatusCode::SUCCESS;
}

