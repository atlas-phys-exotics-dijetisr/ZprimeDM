#include <ZprimeDM/PhotonTriggerHists.h>

PhotonTriggerHists :: PhotonTriggerHists (const std::string& name, const std::string& detailStr, const std::string& jetDetailStr)
  : HistogramManager(name, detailStr),
    m_jetDetailStr(jetDetailStr)
{}

PhotonTriggerHists :: ~PhotonTriggerHists () 
{}

void PhotonTriggerHists::record(EL::IWorker* wk)
{
  HistogramManager::record(wk);

  if(!m_jetDetailStr.empty())
    {
      m_trigJet0    ->record(wk);
      m_trigJet1    ->record(wk);
      m_trigJet2    ->record(wk);
    }
}

StatusCode PhotonTriggerHists::initialize()
{
  if(!m_jetDetailStr.empty())
    {
      m_trigJet0=new ZprimeDM::JetHists(m_name+"trigJet0_" , m_jetDetailStr, "leading trigger");
      ANA_CHECK(m_trigJet0->initialize());

      m_trigJet1=new ZprimeDM::JetHists(m_name+"trigJet1_" , m_jetDetailStr, "subleading trigger");
      ANA_CHECK(m_trigJet1->initialize());

      m_trigJet2=new ZprimeDM::JetHists(m_name+"trigJet2_" , m_jetDetailStr, "third trigger");
      ANA_CHECK(m_trigJet2->initialize());
    }

  h_dRPhotonTrigJet0= book(m_name, "dRPhotonTrigJet0","#DeltaR_{#gamma,lead-trigjet}",100,0,5);
  h_dRPhotonTrigJet1= book(m_name, "dRPhotonTrigJet1","#DeltaR_{#gamma,subl-trigjet}",100,0,5);
  h_dRPhotonTrigJet2= book(m_name, "dRPhotonTrigJet2","#DeltaR_{#gamma,thrd-trigjet}",100,0,5);

  h_dRPhotonTrigJet0_50= book(m_name, "dRPhotonTrigJet0_50","#DeltaR_{#gamma,lead-trigjet-abv50GeV}",100,0,5);
  h_dRPhotonTrigJet1_50= book(m_name, "dRPhotonTrigJet1_50","#DeltaR_{#gamma,subl-trigjet-abv50GeV}",100,0,5);
  h_dRPhotonTrigJet2_50= book(m_name, "dRPhotonTrigJet2_50","#DeltaR_{#gamma,thrd-trigjet-abv50GeV}",100,0,5);

  return StatusCode::SUCCESS;
}

StatusCode PhotonTriggerHists::execute(const xAH::Photon* photon, const xAH::Jet* trigJet0, const xAH::Jet* trigJet1, const xAH::Jet* trigJet2, float eventWeight)
{
  if(!m_jetDetailStr.empty())
    {
      if(trigJet0) ANA_CHECK(m_trigJet0->execute(trigJet0, eventWeight));
      if(trigJet1) ANA_CHECK(m_trigJet1->execute(trigJet0, eventWeight));
      if(trigJet2) ANA_CHECK(m_trigJet2->execute(trigJet0, eventWeight));
    }

  if(trigJet0) h_dRPhotonTrigJet0->Fill(photon->p4.DeltaR(trigJet0->p4),eventWeight);
  if(trigJet1) h_dRPhotonTrigJet1->Fill(photon->p4.DeltaR(trigJet1->p4),eventWeight);
  if(trigJet2) h_dRPhotonTrigJet2->Fill(photon->p4.DeltaR(trigJet2->p4),eventWeight);

  if(trigJet0 && trigJet0->p4.Pt()>50) h_dRPhotonTrigJet0_50->Fill(photon->p4.DeltaR(trigJet0->p4),eventWeight);
  if(trigJet1 && trigJet1->p4.Pt()>50) h_dRPhotonTrigJet1_50->Fill(photon->p4.DeltaR(trigJet1->p4),eventWeight);
  if(trigJet2 && trigJet2->p4.Pt()>50) h_dRPhotonTrigJet2_50->Fill(photon->p4.DeltaR(trigJet2->p4),eventWeight);

  return StatusCode::SUCCESS;
}
