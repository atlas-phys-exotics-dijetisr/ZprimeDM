#include <ZprimeDM/DiphotonHists.h>


DiphotonHists :: DiphotonHists (const std::string& name, const std::string& detailStr, const std::string& jetDetailStr, const std::string& photonDetailStr)
  : EventHists(name, detailStr),
    m_reso(0),
    m_jetDetailStr(jetDetailStr), h_nJet(0), m_jet0(0), m_jet1(0), m_jet2(0),
    m_photonDetailStr(photonDetailStr), h_nPhoton(0), m_photon0(0)
{ }

DiphotonHists :: ~DiphotonHists () 
{ }

void DiphotonHists::record(EL::IWorker* wk)
{
  EventHists::record(wk);

  m_reso        ->record(wk);

  if(!m_jetDetailStr.empty())
    {
      m_jet0    ->record(wk);
      m_jet1    ->record(wk);
      m_jet2    ->record(wk);
    }

  if(!m_photonDetailStr.empty())
    {
      m_photon0 ->record(wk);
      m_photon1 ->record(wk);
      m_photon2 ->record(wk);
    }
}

StatusCode DiphotonHists::initialize()
{
  if(m_debug) std::cout << "DiphotonHists::initialize()" << std::endl;

  ANA_CHECK(EventHists::initialize());

  m_reso        =new ResonanceHists(m_name+"reso_", "");
  ANA_CHECK(m_reso->initialize());

  if(!m_jetDetailStr.empty())
    {
      h_nJet     = book(m_name, "nJets",     "N_{jets}",      10,     -0.5,     9.5 );

      m_jet0=new ZprimeDM::JetHists(m_name+"jet0_" , m_jetDetailStr, "leading");
      ANA_CHECK(m_jet0->initialize());

      m_jet1=new ZprimeDM::JetHists(m_name+"jet1_" , m_jetDetailStr, "subleading");
      ANA_CHECK(m_jet1->initialize());

      m_jet2=new ZprimeDM::JetHists(m_name+"jet2_" , m_jetDetailStr, "third");
      ANA_CHECK(m_jet2->initialize());
    }

  if(!m_photonDetailStr.empty())
    {
      h_nPhoton  = book(m_name, "nPhotons",  "N_{#gamma}",    10,     -0.5,     9.5 );

      m_photon0 =new ZprimeDM::PhotonHists(m_name+"photon0_", m_photonDetailStr, "leading");
      ANA_CHECK(m_photon0->initialize());

      m_photon1 =new ZprimeDM::PhotonHists(m_name+"photon1_", m_photonDetailStr, "subleading");
      ANA_CHECK(m_photon1->initialize());

      m_photon2 =new ZprimeDM::PhotonHists(m_name+"photon2_", m_photonDetailStr, "third");
      ANA_CHECK(m_photon2->initialize());
    }

  return StatusCode::SUCCESS;
}

StatusCode DiphotonHists::execute(const DijetISREvent& event, const xAH::Photon* reso0, const xAH::Photon* reso1, float eventWeight)
{
  if(m_debug) std::cout << "DiphotonHists::execute()" << std::endl;
  ANA_CHECK(EventHists::execute(event, eventWeight));

  ANA_CHECK(m_reso     ->execute(reso0, reso1, eventWeight));

  if(!m_jetDetailStr.empty())
    {
      h_nJet   ->Fill(event.jets()   , eventWeight);

      if(event.jets()>0)    ANA_CHECK(m_jet0   ->execute(event.jet(0)   , eventWeight));
      if(event.jets()>1)    ANA_CHECK(m_jet1   ->execute(event.jet(1)   , eventWeight));
      if(event.jets()>2)    ANA_CHECK(m_jet2   ->execute(event.jet(2)   , eventWeight));
    }

  if(!m_photonDetailStr.empty())
    {
      h_nPhoton->Fill(event.photons(), eventWeight);

      if(event.photons()>0) ANA_CHECK(m_photon0->execute(event.photon(0), eventWeight));
      if(event.photons()>1) ANA_CHECK(m_photon1->execute(event.photon(1), eventWeight));
      if(event.photons()>2) ANA_CHECK(m_photon2->execute(event.photon(2), eventWeight));
    }

  return StatusCode::SUCCESS;
}

StatusCode DiphotonHists::finalize()
{
  if(m_debug) std::cout << "DiphotonHists::finalize()" << std::endl;
  
  ANA_CHECK(EventHists::finalize());

  ANA_CHECK(m_reso     ->finalize());
  delete m_reso;

  if(!m_jetDetailStr.empty())
    {
      ANA_CHECK(m_jet0   ->finalize());
      ANA_CHECK(m_jet1   ->finalize());
      ANA_CHECK(m_jet2   ->finalize());

      delete m_jet0;
      delete m_jet1;
      delete m_jet2;
    }

  if(!m_photonDetailStr.empty())
    {
      ANA_CHECK(m_photon0->finalize());
      ANA_CHECK(m_photon1->finalize());
      ANA_CHECK(m_photon2->finalize());

      delete m_photon0;
      delete m_photon1;
      delete m_photon2;
    }

  return StatusCode::SUCCESS;
}
