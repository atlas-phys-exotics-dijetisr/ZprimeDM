#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

#include <AsgTools/MessageCheck.h>

#include <ZprimeDM/DiphotonHistsAlgo.h>
#include <xAODAnaHelpers/HelperFunctions.h>

#include "TFile.h"
#include "TKey.h"
#include "TLorentzVector.h"
#include "TSystem.h"

#include <utility>      
#include <iostream>
#include <fstream>

using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(DiphotonHistsAlgo)

DiphotonHistsAlgo :: DiphotonHistsAlgo () :
  m_mc(false),
  m_jetDetailStr(""),
  m_photonDetailStr(""),
  m_doPUReweight(false),
  m_doCleaning(false),
  m_jetPtCleaningCut(25.),
  m_doTrigger(false),
  m_dumpTrig(false),
  m_trigger(""),
  m_photon0PtCut(25.),
  m_photon1PtCut(25.),
  m_scalarCuts(false),
  m_YStarCut(-1),
  hIncl(nullptr),
  hMaa600_700(nullptr),
  hMaa700_800(nullptr),
  hMaa800_900(nullptr)
{
  ANA_MSG_INFO("DiphotonHistsAlgo::GammaJetHistsAlgo()");
}

EL::StatusCode DiphotonHistsAlgo :: histInitialize ()
{
  ANA_CHECK(xAH::Algorithm::algInitialize());
  ANA_MSG_INFO("DiphotonHistsAlgo::histInitialize()");

  //
  // data model
  m_event=DijetISREvent::global();

  //
  // Cutflow
  m_cutflow=new CutflowHists(m_name, "");
  ANA_CHECK(m_cutflow->initialize());

  m_cf_trigger =m_cutflow->addCut("trigger");
  m_cf_cleaning=m_cutflow->addCut("cleaning");
  m_cf_photon0 =m_cutflow->addCut("photon0");
  m_cf_photon1 =m_cutflow->addCut("photon1");
  if(m_scalarCuts)
    {
      m_cf_photon0maa=m_cutflow->addCut("photon0maa");
      m_cf_photon1maa=m_cutflow->addCut("photon1maa");
    }
  if(m_YStarCut>0) m_cf_ystar=m_cutflow->addCut("ystar");
  m_cutflow->record(wk());

  //
  // Histograms
  hIncl      =new DiphotonHists(m_name, "", m_jetDetailStr, m_photonDetailStr);
  ANA_CHECK(hIncl->initialize());
  hIncl->record(wk());

  hMaa600_700=new DiphotonHists(m_name+"/Maa700to800/", "", m_jetDetailStr, m_photonDetailStr);
  ANA_CHECK(hMaa600_700->initialize());
  hMaa600_700->record(wk());

  hMaa700_800=new DiphotonHists(m_name+"/Maa700to800/", "", m_jetDetailStr, m_photonDetailStr);
  ANA_CHECK(hMaa700_800->initialize());
  hMaa700_800->record(wk());

  hMaa800_900=new DiphotonHists(m_name+"/Maa700to800/", "", m_jetDetailStr, m_photonDetailStr);
  ANA_CHECK(hMaa800_900->initialize());
  hMaa800_900->record(wk());

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode DiphotonHistsAlgo :: initialize ()
{
  ANA_MSG_DEBUG("DiphotonHistsAlgo::initialize()");

  // Trigger
  std::string token;
  std::istringstream ss(m_trigger);

  while(std::getline(ss, token, ','))
    m_triggers.push_back(token);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode DiphotonHistsAlgo :: execute ()
{
  ANA_MSG_DEBUG("DiphotonHistsAlgo::execute()");

  //
  // Cuts
  float eventWeight    =m_event->m_weight;
  if(m_mc && m_doPUReweight)
    eventWeight *= m_event->m_weight_pileup;

  //
  // do trigger
  if(m_doTrigger)
    {
      ANA_MSG_DEBUG("Doing Trigger");

      if(m_dumpTrig)
	{
	  for(std::string& thisTrig:*m_event->m_passedTriggers)
	    ANA_MSG_DEBUG("\t" << thisTrig);
	}

      //
      // trigger
      bool passTrigger=true;
      for(const std::string& trigger : m_triggers)
	passTrigger &= (std::find(m_event->m_passedTriggers->begin(), m_event->m_passedTriggers->end(), trigger ) != m_event->m_passedTriggers->end());

      if(!passTrigger)
	{
	  ANA_MSG_DEBUG(" Fail Trigger");
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_trigger, eventWeight);
    }

  //
  // doing cleaning
  //
  bool  passCleaning   = true;
  if(!m_event->m_doTruthOnly)
    {
      for(unsigned int i = 0;  i<m_event->jets(); ++i)
	{
	  const xAH::Jet* jet=m_event->jet(i);
	  if(jet->p4.Pt() > m_jetPtCleaningCut)
	    {
	      if(!m_doCleaning && !jet->clean_passLooseBad)
		{
		  ANA_MSG_DEBUG(" Skipping jet clean");
		  continue;
		}
	      if(!jet->clean_passLooseBad) passCleaning = false;
	    }
	  else
	    break;
	}
    }

  //
  //  Jet Cleaning 
  //
  if(m_doCleaning && !passCleaning)
    {
      ANA_MSG_DEBUG(" Fail cleaning");
      return EL::StatusCode::SUCCESS;
    }
  m_cutflow->execute(m_cf_cleaning, eventWeight);

  //
  // Photon part of the selection
  //
  const xAH::Photon* photon0=m_event->photon(0);
  const xAH::Photon* photon1=m_event->photon(1);

  if(photon0->p4.Pt() < m_photon0PtCut)
    {
      ANA_MSG_DEBUG(" Fail PhotonPt0");
      return EL::StatusCode::SUCCESS;
    }
  m_cutflow->execute(m_cf_photon0,eventWeight);

  if(photon1->p4.Pt() < m_photon1PtCut)
    {
      ANA_MSG_DEBUG(" Fail PhotonPt1");
      return EL::StatusCode::SUCCESS;
    }
  m_cutflow->execute(m_cf_photon1,eventWeight);


  TLorentzVector jj = ( photon0->p4 + photon1->p4 );
  float maa=jj.M();

  //
  // Scalar cuts
  //
  if(m_scalarCuts)
    {
      if(photon0->p4.Et()/maa < 0.4)
	{
	  ANA_MSG_DEBUG(" Fail PhotonEt0maa");
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_photon0maa,eventWeight);

      if(photon1->p4.Et()/maa < 0.3)
	{
	  ANA_MSG_DEBUG(" Fail PhotonEt1maa");
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_photon1maa,eventWeight);
    }

  //
  // ystar cut
  //
  if(m_YStarCut>0)
    {
      float yStar = ( photon0->p4.Rapidity() - photon1->p4.Rapidity() ) / 2.0;
      if(fabs(yStar) > m_YStarCut){
	ANA_MSG_DEBUG(" Fail Ystar");
	return EL::StatusCode::SUCCESS;
      }
      m_cutflow->execute(m_cf_ystar,eventWeight);
    }

  ANA_MSG_DEBUG(" Pass All Cut");

  //
  //Filling
  hIncl->execute(*m_event, photon0, photon1, eventWeight);

  if(maa > 600 && maa < 700)
    hMaa600_700->execute(*m_event, photon0, photon1, eventWeight);

  if(maa > 700 && maa < 800)
    hMaa700_800->execute(*m_event, photon0, photon1, eventWeight);

  if(maa > 800 && maa < 900)
    hMaa800_900->execute(*m_event, photon0, photon1, eventWeight);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode DiphotonHistsAlgo :: histFinalize ()
{
  ANA_CHECK(m_cutflow->finalize());
  delete m_cutflow;

  ANA_CHECK(hIncl->finalize());
  delete hIncl;

  ANA_CHECK(hMaa600_700->finalize());
  delete hMaa600_700;

  ANA_CHECK(hMaa700_800->finalize());
  delete hMaa700_800;

  ANA_CHECK(hMaa800_900->finalize());
  delete hMaa800_900;

  return EL::StatusCode::SUCCESS;
}

