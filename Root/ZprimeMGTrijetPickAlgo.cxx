#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

#include <AsgTools/MessageCheck.h>

#include <ZprimeDM/ZprimeMGTrijetPickAlgo.h>
#include <xAODAnaHelpers/HelperFunctions.h>

#include "TFile.h"
#include "TKey.h"
#include "TLorentzVector.h"
#include "TSystem.h"

#include <utility>      
#include <iostream>
#include <fstream>

using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(ZprimeMGTrijetPickAlgo)

ZprimeMGTrijetPickAlgo :: ZprimeMGTrijetPickAlgo () :
  m_truthDetailStr(""),
  m_pickMode("truth"),
  m_jetPtCut(25.),
  m_leadJetPtCut(25.),
  m_ystarCut(-1)
{
  ANA_MSG_INFO("ZprimeMGTrijetPickAlgo::ZprimeMGTrijetPickAlgo()");
}

EL::StatusCode ZprimeMGTrijetPickAlgo :: histInitialize ()
{
  ANA_CHECK(xAH::Algorithm::algInitialize());
  ANA_MSG_INFO("ZprimeMGTrijetPickAlgo::histInitialize()");

  //
  // data model
  m_event=DijetISREvent::global();

  //
  // Cutflow
  m_cutflow=new CutflowHists(m_name, "");
  ANA_CHECK(m_cutflow->initialize());

  m_cf_jets =m_cutflow->addCut("jets");
  m_cf_jet0 =m_cutflow->addCut("jet0");
  if(m_ystarCut>0) m_cf_ystar=m_cutflow->addCut("ystar");

  m_cutflow->record(wk());

  //
  // Histograms
  h_mjj=new ZprimeMGTrijetHists(m_name+"/mjj/", "");
  ANA_CHECK(h_mjj->initialize());
  h_mjj->record(wk());

  h_mjj_correct  =new ZprimeMGTrijetHists(m_name+"/mjj_correct/",     "");
  ANA_CHECK(h_mjj_correct  ->initialize());
  h_mjj_correct  ->record(wk());

  h_mjj_incorrect=new ZprimeMGTrijetHists(m_name+"/mjj_incorrect/",   "");
  ANA_CHECK(h_mjj_incorrect->initialize());
  h_mjj_incorrect->record(wk());

  std::stringstream ss;
  for(uint binIdx=0;binIdx<10;binIdx++)
    {
      uint hi=150+binIdx*100;
      uint lo= 50+binIdx*100;

      ss.str(""); ss.clear();
      ss << m_name << "/" << "bin" << lo << "to" << hi << "/correct/";
      h_mjj_binscor[binIdx]=new ZprimeMGTrijetHists(ss.str(), "");
      ANA_CHECK(h_mjj_binscor[binIdx]->initialize());
      h_mjj_binscor[binIdx]->record(wk());

      ss.str(""); ss.clear();
      ss << m_name << "/" << "bin" << lo << "to" << hi << "/incorrect/";
      h_mjj_binsinc[binIdx]=new ZprimeMGTrijetHists(ss.str(), "");
      ANA_CHECK(h_mjj_binsinc[binIdx]->initialize());
      h_mjj_binsinc[binIdx]->record(wk());
    }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZprimeMGTrijetPickAlgo :: execute ()
{
  ANA_MSG_DEBUG("ZprimeMGTrijetPickAlgo::execute()");

  //
  // Cuts
  float eventWeight    =m_event->m_weight;

  //
  // Identify particles
  const xAH::TruthPart *jets[3];

  uint lastJetIdx=0;
  for(uint truthIdx=0; truthIdx < m_event->truths(); truthIdx++)
    {
      const xAH::TruthPart *truth=m_event->truth(truthIdx);
      if(truth->status==1)
	{
	  jets       [lastJetIdx]=truth;
	  lastJetIdx++;
	}
    }

  std::sort(std::begin(jets),std::end(jets),ZprimeHelperClasses::TruthSorter::sort);

  // PID the new order
  bool  jets_isReso[3];
  for(uint jetIdx=0; jetIdx < 3; jetIdx++)
    {
      uint parentPdgId=jets[jetIdx]->parent_pdgId[0];
      jets_isReso[jetIdx]=(parentPdgId==101);
    }


  //
  // Pick elements
  uint jet_isr_idx  =0;
  uint jet_reso0_idx=1;
  uint jet_reso1_idx=2;
  if(m_pickMode=="etasort")
    {
      double eta0=fabs(jets[0]->p4.Eta());
      double eta1=fabs(jets[1]->p4.Eta());
      double eta2=fabs(jets[2]->p4.Eta());

      if(eta0<eta1 && eta0<eta2)
	{
	  jet_reso0_idx=0;
	  if(eta1<eta2)
	    {
	      jet_reso1_idx=1;
	      jet_isr_idx  =2;
	    }
	  else
	    {
	      jet_reso1_idx=2;
	      jet_isr_idx  =1;
	    }
	}
      else if(eta1<eta0 && eta1<eta2)
	{
	  jet_reso0_idx=1;
	  if(eta0<eta2)
	    {
	      jet_reso1_idx=0;
	      jet_isr_idx  =2;
	    }
	  else
	    {
	      jet_reso1_idx=2;
	      jet_isr_idx  =0;
	    }
	}
      else if(eta2<eta0 && eta2<eta1)
	{
	  jet_reso0_idx=2;
	  if(eta0<eta1)
	    {
	      jet_reso1_idx=0;
	      jet_isr_idx  =1;
	    }
	  else
	    {
	      jet_reso1_idx=1;
	      jet_isr_idx  =0;
	    }
	}
    }
  else if(m_pickMode=="minystar")
    {
      double ystar12=fabs(jets[0]->p4.Eta()-jets[1]->p4.Eta());
      double ystar13=fabs(jets[0]->p4.Eta()-jets[2]->p4.Eta());
      double ystar23=fabs(jets[1]->p4.Eta()-jets[2]->p4.Eta());
      if(ystar12<ystar13 && ystar12<ystar23)
	{
	  jet_reso0_idx=0;
	  jet_reso1_idx=1;
	  jet_isr_idx  =2;
	}
      else if(ystar13<ystar12 && ystar13<ystar23)
	{
	  jet_reso0_idx=0;
	  jet_reso1_idx=2;
	  jet_isr_idx  =1;
	}
      else
	{
	  jet_reso0_idx=1;
	  jet_reso1_idx=2;
	  jet_isr_idx  =0;
	}
    }
  else if(m_pickMode=="maxzpt")
    {
      TLorentzVector pt12=jets[0]->p4+jets[1]->p4;
      TLorentzVector pt13=jets[0]->p4+jets[2]->p4;
      TLorentzVector pt23=jets[1]->p4+jets[2]->p4;
      if(pt12.Pt()>pt13.Pt() && pt12.Pt()>pt23.Pt())
	{
	  jet_reso0_idx=0;
	  jet_reso1_idx=1;
	  jet_isr_idx  =2;
	}
      else if(pt13.Pt()>pt12.Pt() && pt13.Pt()>pt23.Pt())
	{
	  jet_reso0_idx=0;
	  jet_reso1_idx=2;
	  jet_isr_idx  =1;
	}
      else
	{
	  jet_reso0_idx=1;
	  jet_reso1_idx=2;
	  jet_isr_idx  =0;
	}
    }
  else if(m_pickMode.compare(0,1,"m")==0)
    {
      jet_reso0_idx=m_pickMode[1]-1-'0';
      jet_reso1_idx=m_pickMode[2]-1-'0';

      if     (jet_reso0_idx==0 && jet_reso1_idx==1) jet_isr_idx=2;
      else if(jet_reso0_idx==0 && jet_reso1_idx==2) jet_isr_idx=1;
      else if(jet_reso0_idx==1 && jet_reso1_idx==2) jet_isr_idx=0;
    }
  else if(m_pickMode.compare(0,1,"d")==0)
    {
      bool usedR  =(m_pickMode.compare(1,1,"R")==0);
      bool usedPhi=(m_pickMode.compare(1,3,"Phi")==0);
      bool usedEta=(m_pickMode.compare(1,3,"Eta")==0);

      bool usemin=(m_pickMode.compare(m_pickMode.length()-3,3,"min")==0);

      double d12;
      double d13;
      double d23;
      if(usedR)
	{
	  d12=jets[0]->p4.DeltaR(jets[1]->p4);
	  d13=jets[0]->p4.DeltaR(jets[2]->p4);
	  d23=jets[1]->p4.DeltaR(jets[2]->p4);
	}
      else if(usedPhi)
	{
	  d12=fabs(jets[0]->p4.DeltaPhi(jets[1]->p4));
	  d13=fabs(jets[0]->p4.DeltaPhi(jets[2]->p4));
	  d23=fabs(jets[1]->p4.DeltaPhi(jets[2]->p4)); 
	}
      else if(usedEta)
	{
	  d12=fabs(jets[0]->p4.Eta()-jets[1]->p4.Eta());
	  d13=fabs(jets[0]->p4.Eta()-jets[2]->p4.Eta());
	  d23=fabs(jets[1]->p4.Eta()-jets[2]->p4.Eta()); 
	}
      else
	{
	  std::cout << "Unknown dX mode!" << std::endl;
	  return EL::StatusCode::FAILURE;
	}

      if(usemin)
	{
	  if(d12<min(d13,d23))
	    {
	      jet_isr_idx  =2;
	      jet_reso0_idx=0;
	      jet_reso1_idx=1;
	    }
	  else if(d13<min(d12,d23))
	    {
	      jet_isr_idx  =1;
	      jet_reso0_idx=0;
	      jet_reso1_idx=2;
	    }
	  else // if(d23<min(d12,d13))
	    {
	      jet_isr_idx  =0;
	      jet_reso0_idx=1;
	      jet_reso1_idx=2;
	    }
	}
      else
	{
	  if(d12>max(d13,d23))
	    {
	      jet_isr_idx  =2;
	      jet_reso0_idx=0;
	      jet_reso1_idx=1;
	    }
	  else if(d13>max(d12,d23))
	    {
	      jet_isr_idx  =1;
	      jet_reso0_idx=0;
	      jet_reso1_idx=2;
	    }
	  else // if(d23>max(d12,d13))
	    {
	      jet_isr_idx  =0;
	      jet_reso0_idx=1;
	      jet_reso1_idx=2;
	    }
	}
    }
  else // if(m_pickMode=="truth")
    {
      lastJetIdx=0;
      for(uint jetIdx=0;jetIdx<3;jetIdx++)
  	{
  	  if(jets_isReso[jetIdx])
  	    {
  	      if(lastJetIdx==0)
  		{
  		  jet_reso0_idx=jetIdx;
  		  lastJetIdx++;
  		}
  	      else
  		jet_reso1_idx=jetIdx;
  	    }
  	  else
  	    jet_isr_idx=jetIdx;
  	}
    }

  // assign objects and determine correctness
  const xAH::TruthPart* jet_isr  =jets[jet_isr_idx];
  const xAH::TruthPart* jet_reso0=jets[jet_reso0_idx];
  const xAH::TruthPart* jet_reso1=jets[jet_reso1_idx];
  bool correct   =jets_isReso[jet_reso0_idx] && jets_isReso[jet_reso1_idx];

  //
  // Need three jets  
  for(uint jetIdx=0;jetIdx<3;jetIdx++)
    {
      if(jets[jetIdx]->p4.Pt() < m_jetPtCut)
  	{
  	  ANA_MSG_DEBUG(" Fail jet (idx = " << jetIdx << ")");
  	  return EL::StatusCode::SUCCESS;
  	}
    }
  m_cutflow->execute(m_cf_jets,eventWeight);

  //
  // Need jet0
  if(jets[0]->p4.Pt() < m_leadJetPtCut)
    {
      ANA_MSG_DEBUG(" Fail lead jet");
      return EL::StatusCode::SUCCESS;
    }
  m_cutflow->execute(m_cf_jet0,eventWeight);

  //
  // Need ystar
  if(m_ystarCut>0)
    {
      double ystarjj =fabs(jet_reso0->p4.Rapidity()-jet_reso1->p4.Rapidity())/2.;
      if(ystarjj>m_ystarCut)
  	{
  	  ANA_MSG_DEBUG(" Fail ystar");
  	  return EL::StatusCode::SUCCESS;
  	}
      m_cutflow->execute(m_cf_ystar,eventWeight);
    }

  //
  // Histograms
  if(correct)
    h_mjj_correct  ->execute(jet_reso0,jet_reso1,jet_isr,eventWeight);
  else
    h_mjj_incorrect->execute(jet_reso0,jet_reso1,jet_isr,eventWeight);
  h_mjj->execute(jet_reso0,jet_reso1,jet_isr,eventWeight);

  // binning
  TLorentzVector p4_Zprime=jet_reso0->p4+jet_reso1->p4;
  int bin=floor((p4_Zprime.M()-50.)/100.);
  if(0<=bin && bin<=9)
    {
      if(correct)
	h_mjj_binscor[bin]->execute(jet_reso0,jet_reso1,jet_isr, eventWeight);
      else
	h_mjj_binsinc[bin]->execute(jet_reso0,jet_reso1,jet_isr, eventWeight);
    }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZprimeMGTrijetPickAlgo :: histFinalize ()
{
  ANA_CHECK(m_cutflow->finalize());
  delete m_cutflow;

  ANA_CHECK(h_mjj->finalize());
  delete h_mjj;

  ANA_CHECK(h_mjj_correct  ->finalize());
  delete h_mjj_correct;

  ANA_CHECK(h_mjj_incorrect->finalize());
  delete h_mjj_incorrect;

  for(uint binIdx=0;binIdx<10;binIdx++)
    {
      ANA_CHECK(h_mjj_binscor[binIdx]->finalize());
      delete h_mjj_binscor[binIdx];

      ANA_CHECK(h_mjj_binsinc[binIdx]->finalize());
      delete h_mjj_binsinc[binIdx];	  
    }

  return EL::StatusCode::SUCCESS;
}
