#include <ZprimeDM/ZprimeDijetHistsAlgo.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODAnaHelpers/HelperClasses.h>

// this is needed to distribute the algorithm to the workers
ClassImp(ZprimeDijetHistsAlgo)

ZprimeDijetHistsAlgo :: ZprimeDijetHistsAlgo ()
: ZprimeHistsBaseAlgo()
{
  m_resoJet0Idx =0;
  m_resoJet1Idx =1;

  m_nJets=2;
  m_minJetPt=25.;
}

void ZprimeDijetHistsAlgo :: initISRCutflow ()
{
  m_cf_njets = m_cutflow->addCut("NJets");
}

bool ZprimeDijetHistsAlgo :: doISRCutflow ()
{
  //
  // njet
  uint njets = 0;
  for(unsigned int i = 0;  i<m_event->jets(); ++i)
    if(m_event->jet(i)->p4.Pt()>m_minJetPt) njets++;
    else break;

  if(njets < m_nJets)
    {
      ANA_MSG_DEBUG(" Fail NJets with " << njets);
      return false;
    }
  m_cutflow->execute(m_cf_njets,m_eventWeight);

  //
  // Setup objects
  //
  m_reso0=m_event->jet(m_resoJet0Idx);
  m_reso1=m_event->jet(m_resoJet1Idx);
  m_isr  =0;

  return true;
}
