#include <ZprimeDM/ZprimeTrijetHistsAlgo.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODAnaHelpers/HelperClasses.h>

// this is needed to distribute the algorithm to the workers
ClassImp(ZprimeTrijetHistsAlgo)

ZprimeTrijetHistsAlgo :: ZprimeTrijetHistsAlgo ()
: ZprimeHistsBaseAlgo()
{
  m_minLeadingJetPt = 410;
}

void ZprimeTrijetHistsAlgo :: initISRCutflow ()
{
  m_cf_njets = m_cutflow->addCut("NJets");
  m_cf_leadingjet = m_cutflow->addCut("LeadJet");
}

bool ZprimeTrijetHistsAlgo :: doISRCutflow ()
{
  ANA_MSG_DEBUG("Calling ZprimeTrijetHistsAlgo::doISRCutflow()");
  //
  // trijet
  uint njets = m_event->jets();

  if(njets < 3)
    {
      ANA_MSG_DEBUG(" Fail NJets with " << njets);
      return false;
    }
  m_cutflow->execute(m_cf_njets,m_eventWeight);

  //
  // leading jet
  if(m_event->jet(0)->p4.Pt() < m_minLeadingJetPt)
    {
      ANA_MSG_DEBUG(" Fail JetPt0 with " << m_event->jet(0)->p4.Pt());
      return false;
    }
  m_cutflow->execute(m_cf_leadingjet,m_eventWeight);

  //
  // Setup objects
  //
  m_reso0=m_event->jet(1);
  m_reso1=m_event->jet(2);
  m_isr  =m_event->jet(0);

  return true;
}
