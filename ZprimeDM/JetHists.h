#ifndef ZprimeDM_JetHists_H
#define ZprimeDM_JetHists_H

#include <xAODAnaHelpers/HistogramManager.h>

#include <ZprimeDM/DijetISREvent.h>

namespace ZprimeDM
{
  class JetHists : public HistogramManager
  {
  public:

    JetHists(const std::string& name, const std::string& detailStr, const std::string& prefix="");
    virtual ~JetHists() ;

    bool m_debug;
    virtual StatusCode initialize();

    StatusCode execute(const xAH::Jet*      jet, float eventWeight);
    using HistogramManager::book;    // make other overloaded version of book() to show up in subclass
    using HistogramManager::execute; // make other overloaded version of execute() to show up in subclass

  private:
    HelperClasses::JetInfoSwitch m_infoSwitch;

    std::string m_prefix;

    //histograms

    // kinematic
    TH1F* h_pt;
    TH1F* h_pt_m;
    TH1F* h_pt_l;
    TH1F* h_eta;
    TH1F* h_phi;
    TH1F* h_m;

    TH1F* h_vr;

    // clean
    TH1F* h_Timing;
    TH1F* h_LArQuality;
    TH1F* h_HECQuality;
    TH1F* h_NegativeE;
    TH1F* h_AverageLArQF;
    TH1F* h_LArQmean;
    TH1F* h_BchCorrCell;
    TH1F* h_N90Constituents;
    TH1F* h_LArBadHVEnergyFrac;
    TH1F* h_LArBadHVNCell;
    TH1F* h_OotFracClusters5;
    TH1F* h_OotFracClusters10;
    TH1F* h_LeadingClusterPt;
    TH1F* h_LeadingClusterSecondLambda;
    TH1F* h_LeadingClusterCenterLambda;
    TH1F* h_LeadingClusterSecondR;
    TH1F* h_clean_passLooseBad;
    TH1F* h_clean_passLooseBadUgly;
    TH1F* h_clean_passTightBad;
    TH1F* h_clean_passTightBadUgly;

    // energy
    TH1F* h_HECFrac;
    TH1F* h_EMFrac;
    TH1F* h_CentroidR;
    TH1F* h_FracSamplingMax;
    TH1F* h_FracSamplingMaxIndex;
    TH1F* h_LowEtConstituentsFrac;
    TH1F* h_GhostMuonSegmentCount;
    TH1F* h_Width;

    // trackPV
    TH1F* h_NumTrkPt1000PV;
    TH1F* h_SumPtTrkPt1000PV;
    TH1F* h_TrackWidthPt1000PV;
    TH1F* h_NumTrkPt500PV;
    TH1F* h_SumPtTrkPt500PV;
    TH1F* h_TrackWidthPt500PV;
    TH1F* h_JVFPV;

    // trackAll or trackPV
    TH1F* h_Jvt;
    TH1F* h_JvtJvfcorr;
    TH1F* h_JvtRpt;

    // flavorTag
    TH1F* h_SV0;
    TH1F* h_SV1;
    TH1F* h_IP3D;
    TH1F* h_SV1plusIP3D_discriminant;
    TH1F* h_MV1;
    TH1F* h_MV2c00;
    TH1F* h_MV2c10;
    TH1F* h_MV2c10mu;
    TH1F* h_MV2c10rnn;
    TH1F* h_MV2c20;
    TH1F* h_DL1;
    TH1F* h_DL1_pu;
    TH1F* h_DL1_pc;
    TH1F* h_DL1_pb;
    TH1F* h_DL1mu;
    TH1F* h_DL1mu_pu;
    TH1F* h_DL1mu_pc;
    TH1F* h_DL1mu_pb;
    TH1F* h_DL1rnn;
    TH1F* h_DL1rnn_pu;
    TH1F* h_DL1rnn_pc;
    TH1F* h_DL1rnn_pb;
    TH1F* h_HadronConeExclTruthLabelID;

    // truth
    TH1F *h_ConeTruthLabelID;
    TH1F *h_TruthCount;
    TH1F *h_TruthLabelDeltaR_B;
    TH1F *h_TruthLabelDeltaR_C;
    TH1F *h_TruthLabelDeltaR_T;
    TH1F *h_PartonTruthLabelID;
    TH1F *h_GhostTruthAssociationFraction;

    TH1F *h_truth_pt;
    TH1F *h_truth_pt_m;
    TH1F *h_truth_pt_l;

    TH1F *h_truth_eta;
    TH1F *h_truth_phi;

    // charge
    TH1F *h_charge;

  };
}

#endif // ZprimeDM_JetHists_H
