#ifndef ZprimeDM_ElectronHists_H
#define ZprimeDM_ElectronHists_H

#include <xAODAnaHelpers/HistogramManager.h>

#include <ZprimeDM/DijetISREvent.h>

namespace ZprimeDM 
{
  class ElectronHists : public HistogramManager
  {
  public:

    ElectronHists(const std::string& name, const std::string& detailStr, const std::string& prefix="");
    virtual ~ElectronHists() ;

    bool m_debug;
    virtual StatusCode initialize();

    StatusCode execute(const xAH::Electron* electron, float eventWeight);
    using HistogramManager::book;    // make other overloaded version of book() to show up in subclass
    using HistogramManager::execute; // make other overloaded version of execute() to show up in subclass

  private:
    HelperClasses::ElectronInfoSwitch m_infoSwitch;

    std::string m_prefix;

    //histograms

    // kinematic
    TH1F* h_pt;
    TH1F* h_pt_m;
    TH1F* h_pt_l;
    TH1F* h_eta;
    TH1F* h_phi;
    TH1F* h_m;
  };
}

#endif // ZprimeDM_ElectronHists_H
