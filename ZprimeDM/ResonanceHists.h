#ifndef ZprimeDM_ResonanceHists_H
#define ZprimeDM_ResonanceHists_H

#include <xAODAnaHelpers/HistogramManager.h>
#include <xAODAnaHelpers/HelperClasses.h>
#include <xAODJet/Jet.h>

#include <ZprimeDM/DijetISREvent.h>

class ResonanceHists : public HistogramManager
{
public:
  ResonanceHists(const std::string& name, const std::string& detailStr="");
  virtual ~ResonanceHists();

  StatusCode initialize();
  StatusCode execute(const xAH::Particle* reso0, const xAH::Particle* reso1, float eventWeight) ;
  using HistogramManager::book; // make other overloaded version of book() to show up in subclass
  using HistogramManager::execute; // overload

private:
  //basic
  TH1F* h_pt;
  TH1F* h_pt_l;
  TH1F* h_eta;
  TH1F* h_phi;
  TH1F* h_m;
};

#endif
