#ifndef ZprimeDM_ZprimeHistsBaseAlgo_H
#define ZprimeDM_ZprimeHistsBaseAlgo_H

#include <EventLoop/StatusCode.h>
#include <EventLoop/Algorithm.h>
#include <EventLoop/Worker.h>

//algorithm wrapper
#include <xAODAnaHelpers/Algorithm.h>

// our histogramming code
#include <ZprimeDM/DijetISREvent.h>

#include <ZprimeDM/BCIDBugChecker.h>

#include <ZprimeDM/CutflowHists.h>
#include <ZprimeDM/DijetISRHists.h>

// ROOT include(s):
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TLorentzVector.h"

#include <sstream>
#include <vector>

using namespace std;



class ZprimeHistsBaseAlgo : public xAH::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  
  //configuration variables
  bool m_mc;

  // switches
  std::string m_histDetailStr;
  std::string m_jetDetailStr;
  std::string m_photonDetailStr;
  bool m_doDetails;          // Make histograms for exlusive selections selections
  bool m_doTruthOnly;
  bool m_doBCIDCheck;
  bool m_doPUReweight;
  bool m_doCleaning;
  float m_jetPtCleaningCut;
  bool m_doSysts;

  // trigger config
  bool m_doTrigger;  
  bool m_dumpTrig;
  /** @brief Comma-separated list of triggers that must pass (AND) */
  std::string m_trigger;

  // Kinematic selection
  float m_reso0PtCut;
  float m_reso0PtCutMax;
  float m_reso1PtCut;
  uint  m_nresoBtag;
  xAH::Jet::BTaggerOP m_resoBtagAlgo;
  float m_ptjjCut;
  float m_ystarCut;
  float m_YBoostCut;
  float m_dPhijjCut;
  float m_asymjjCut;
  float m_projasymjjCut;
  float m_dRISRclosejCut;
  float m_dPhiISRclosejCut;
  float m_yStarISRjjCut;
  float m_pt1ptisrCut;
  float m_mjjCut;
  
  int m_survivingEvents;

private:

  //
  // Cutflow
  int m_cf_trigger;
  int m_cf_cleaning;
  // ISR cuts
  int m_cf_reso0;
  int m_cf_reso0_max;
  int m_cf_reso1;
  int m_cf_nresoBtag;
  int m_cf_ptjj;
  int m_cf_ystar;
  int m_cf_yboost;
  int m_cf_dphijj;
  int m_cf_asymjj;
  int m_cf_projasymjj;
  int m_cf_drisrclosej;
  int m_cf_dphiisrclosej;
  int m_cf_ystarisrjj;
  int m_cf_pt1ptisr;
  int m_cf_mjj;

  // BCID bug
  BCIDBugChecker *m_BCIDchecker; //!

  //
  // Histograms
  DijetISRHists *hIncl; //!

  DijetISRHists *hMjj100_150; //!
  DijetISRHists *hMjj100_200; //!
  DijetISRHists *hMjj150_250; //!
  DijetISRHists *hMjj200_300; //!
  DijetISRHists *hMjj250_350; //!
  DijetISRHists *hMjj300_400; //!
  DijetISRHists *hMjj350_450; //!
  DijetISRHists *hMjj400_500; //!
  DijetISRHists *hMjj450_550; //!
  DijetISRHists *hMjj500_600; //!
  DijetISRHists *hMjj700_800; //!
  
  //
  // Histograms with varied b-tag
  // weights for systematics
  std::vector<DijetISRHists*> m_bSFSystHists;

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

protected:
  // Cutflow data
  CutflowHists *m_cutflow; //!

  // Event data
  DijetISREvent *m_event; //!

  //
  // Parsed configuration
  std::vector<std::string> m_triggers;

  // ISR selection
  float m_eventWeight;
  const xAH::Jet *m_reso0;    //!
  const xAH::Jet *m_reso1;    //!
  const xAH::Particle *m_isr; //!

  //
  // functions
  virtual void initISRCutflow() =0;
  virtual bool doISRCutflow() =0;

public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  // this is a standard constructor
  ZprimeHistsBaseAlgo ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode histFill (float eventWeight, DijetISRHists * systHist = nullptr);
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(ZprimeHistsBaseAlgo, 1);
};

#endif
