#ifndef ZprimeDM_PartonJetHists_H
#define ZprimeDM_PartonJetHists_H

#include <xAODAnaHelpers/HistogramManager.h>
#include <xAODAnaHelpers/HelperClasses.h>
#include <xAODJet/Jet.h>
#include <xAODTruth/TruthParticle.h>

class PartonJetHists : public HistogramManager
{
public:
  PartonJetHists(const std::string& name);
  virtual ~PartonJetHists();

  StatusCode initialize();
  StatusCode execute( const xAOD::Jet *jet, const xAOD::TruthParticle *parton, float eventWeight);
  using HistogramManager::book; // make other overloaded version of book() to show up in subclass
  using HistogramManager::execute; // overload

private:
  //basic
  TH1F* m_dPt;                  //!
  TH1F* m_dEta;                 //!
  TH1F* m_dPhi;                 //!
  TH1F* m_dR;                   //!

  TH2F* m_ptvspt;               //!
};

#endif
