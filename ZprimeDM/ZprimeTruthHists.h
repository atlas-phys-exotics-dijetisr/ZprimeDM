#ifndef ZprimeDM_ZprimeTruthHists_H
#define ZprimeDM_ZprimeTruthHists_H

#include <xAODTruth/TruthParticleContainer.h>

#include <xAODAnaHelpers/HistogramManager.h>
#include <xAODAnaHelpers/HelperClasses.h>

class ZprimeTruthHists : public HistogramManager
{
public:
  ZprimeTruthHists(const std::string& name);
  virtual ~ZprimeTruthHists();

  StatusCode initialize();
  StatusCode execute( const xAOD::TruthParticleContainer* truthContainer, float eventWeight );
  using HistogramManager::book; // make other overloaded version of book() to show up in subclass
  using HistogramManager::execute; // overload

private:
  //basic
  TH1F* m_Zm;                  //!
  TH1F* m_Zpt;                 //!
  TH1F* m_Zphi;                //!
  TH1F* m_Zeta;                //!
};

#endif
