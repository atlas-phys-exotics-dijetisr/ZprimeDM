#ifndef ZprimeDM_JetStudiesAlgo_H
#define ZprimeDM_JetStudiesAlgo_H

#include <EventLoop/StatusCode.h>
#include <EventLoop/Algorithm.h>
#include <EventLoop/Worker.h>

//algorithm wrapper
#include <xAODAnaHelpers/Algorithm.h>

// our histogramming code
#include <ZprimeDM/CutflowHists.h>
#include <ZprimeDM/JetHists.h>

// ROOT include(s):
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TLorentzVector.h"

#include <sstream>
#include <vector>

using namespace std;



class JetStudiesAlgo : public xAH::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  
  //configuration variables
//  bool m_debug;
  bool m_mc;

  // switches
  std::string m_jetDetailStr;
  bool m_doTruthOnly;
  bool m_doPUReweight;           
  bool m_doCleaning;
  float m_jetPtCleaningCut;

  // trigger config
  bool m_doTrigger;  
  std::string m_trigger;

  // Kinematic selection
  float m_jet0PtCut;
  float m_jet1PtCut;

  // Binning selection
  float m_minPt;
  float m_maxPt;
  float m_minEta;
  float m_maxEta;
private:

  //
  // Cutflow
  int m_cf_trigger;
  int m_cf_cleaning;
  // Kinematic cuts
  int m_cf_jet0;
  int m_cf_jet1;

  // Binning selection
  float m_cf_minpt;
  float m_cf_maxpt;
  float m_cf_mineta;
  float m_cf_maxeta;

  //
  // Histograms
  ZprimeDM::JetHists *hJet; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

protected:
  // Cutflow data
  CutflowHists *m_cutflow; //!

  // Event data
  DijetISREvent *m_event; //!

public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  // this is a standard constructor
  JetStudiesAlgo ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(JetStudiesAlgo, 1);
};

#endif
