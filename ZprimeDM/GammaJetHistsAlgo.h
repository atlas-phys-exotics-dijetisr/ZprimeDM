#ifndef ZprimeDM_GammaJetHistsAlgo_H
#define ZprimeDM_GammaJetHistsAlgo_H

#include <EventLoop/StatusCode.h>
#include <EventLoop/Algorithm.h>
#include <EventLoop/Worker.h>

//algorithm wrapper
#include <xAODAnaHelpers/Algorithm.h>

// our histogramming code
#include <ZprimeDM/DijetISREvent.h>

#include <ZprimeDM/BCIDBugChecker.h>

#include <ZprimeDM/CutflowHists.h>
#include <ZprimeDM/GammaJetHists.h>

// ROOT include(s):
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TLorentzVector.h"

#include <sstream>
#include <vector>

using namespace std;



class GammaJetHistsAlgo : public xAH::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  
  //configuration variables
  bool m_debug;
  bool m_mc;

  // switches
  std::string m_histDetailStr;
  std::string m_jetDetailStr;
  std::string m_photonDetailStr;
  bool m_doDetails;          // Make histograms for exlusive selections selections
  bool m_doTruthOnly;
  bool m_doBCIDCheck;
  bool m_doPUReweight;
  bool m_doCleaning;
  float m_jetPtCleaningCut;

  // trigger config
  bool m_doTrigger;
  bool m_dumpTrig;
  /** @brief Comma-separated list of triggers that must pass (AND) */
  std::string m_trigger;

  // Kinematic selection
  float m_photon0PtCut;
  float m_jet0PtCut;

  float m_ystarCut;

  bool m_phTrigMatch;       // photon must be matched to a trigger object

private:
  //
  // Cutflow
  int m_cf_trigger;
  int m_cf_cleaning;
  // Kinematic cuts
  int m_cf_photon0;
  int m_cf_jet0;
  int m_cf_ystar;
  int m_cf_phTrigMatch;

  // BCID bug
  BCIDBugChecker *m_BCIDchecker; //!

  //
  // Triggers
  std::vector<std::string> m_triggers;

  //
  // Histograms
  GammaJetHists *hIncl; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

protected:
  // Cutflow data
  CutflowHists *m_cutflow; //!

  // Event data
  DijetISREvent *m_event; //!

public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  // this is a standard constructor
  GammaJetHistsAlgo ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(GammaJetHistsAlgo, 1);
};

#endif
