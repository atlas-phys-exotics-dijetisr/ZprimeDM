import ROOT
from xAODAnaHelpers import Config

c = Config()

c.algorithm("BasicEventSelection",    { "m_truthLevelOnly"      : True,
                                        "m_derivationName"      : "TRUTH1",
                                        "m_useMetaData"         : False
                                        } )

c.algorithm("SortAlgo",              { "m_inContainerName"         :  "AntiKt4TruthJets",
                                       "m_outContainerName"        :  "AntiKt4TruthJetsSort"
                                       } )

c.algorithm("JetSelector",                { "m_name"                    :  "SignalJets",
                                            "m_inContainerName"         :  "AntiKt4TruthJetsSort",
                                            "m_outContainerName"        :  "SignalJets",
                                            "m_decorateSelectedObjects" :  False, 
                                            "m_createSelectedContainer" :  True, 
                                            "m_pT_min"                  :  25e3,
                                            "m_eta_max"                 :  2.8,
                                            "m_pass_min"                :  1
                                            } )

c.algorithm("JetPartonHistsAlgo",     { "m_jetContainerName"    : "AntiKt4TruthJets"
                                        } )
