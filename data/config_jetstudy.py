import ROOT
from xAODAnaHelpers import Config

commonsel={"m_mc"                     : args.is_MC,
           "m_debug"                  : False,
           "m_jetDetailStr"           : "kinematic energy clean trackPV charge",
           "m_doPUReweight"           : False,
           "m_doCleaning"             : True,
           "m_jetPtCleaningCut"       : 25,
           "m_doTrigger"              : True
           }

c = Config()

GRL = "GoodRunsLists/data15_13TeV/data15_13TeV.periodAllYear_DetStatus-v75-repro20-01_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml,GoodRunsLists/data16_13TeV/data16_13TeV.periodAllYear_DetStatus-v79-pro20-05_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml"

#
# Process Ntuple
#

c.algorithm("MiniTreeEventSelection", { "m_name"                   : "",
                                        "m_debug"                  : False,
                                        "m_mc"                     : args.is_MC,
                                        "m_GRLxml"                 : GRL,
                                        "m_applyGRL"               : not args.is_MC,
                                        "m_doTruthOnly"            : False,
                                        "m_triggerDetailStr"       : "passTriggers",
                                        "m_jetDetailStr"           : "kinematic energy clean trackPV charge"
                                        } )

#
# bins
#
etabins=[0.8,1.2,2.1,2.5]
ptbins=[('HLT_j15',  40),
        ('HLT_j25',  60),
        ('HLT_j35',  85),
        ('HLT_j60', 115),
        ('HLT_j60', 145),
        ('HLT_j110',175),
        ('HLT_j110',220),
        ('HLT_j175',270),
        ('HLT_j175',330),
        ('HLT_j260',400),
        ('HLT_j360',525)]

for etaIdx in range(len(etabins)+1):
    mineta=-1
    maxeta=-1
    if etaIdx==0:
        maxeta=etabins[0]
    elif etaIdx==len(etabins):
        mineta=etabins[-1]
    else:
        mineta=etabins[etaIdx-1]
        maxeta=etabins[etaIdx]

    etastr='eta'
    if mineta>0: etastr+='%0.1f'%mineta
    etastr+='to'
    if maxeta>0: etastr+='%0.1f'%maxeta

    for ptIdx in range(len(ptbins)):
        minpt=ptbins[ptIdx][1]
        maxpt=-1
        trigger=ptbins[ptIdx][0]
        if ptIdx<len(ptbins)-1:
            maxpt=ptbins[ptIdx+1][1]

        ptstr='pt'
        if minpt>0: ptstr+='%d'%minpt
        ptstr+='to'
        if maxpt>0: ptstr+='%d'%maxpt


        jetstudy=commonsel.copy()
        jetstudy.update({"m_name"    : '%s/%s/'%(ptstr,etastr),
                         "m_trigger" : trigger,
                         "m_minPt"   : minpt,
                         "m_maxPt"   : maxpt,
                         "m_minEta"  : mineta,
                         "m_maxEta"  : maxeta})
        c.algorithm("JetStudiesAlgo", jetstudy )
