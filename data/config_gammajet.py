import ROOT
from xAODAnaHelpers import Config
from ZprimeDM import commonconfig

isSys=False
doDetails=True
histDetail   = "" #2d" #2d debug"
jetDetail    = "kinematic clean layer trackPV useTheS energy flavorTag sfFTagFix%s"%(''.join(['%d'%btagWP for btagWP in commonconfig.btagWPs]))
photonDetail = "kinematic isolation purity PID effSF"
nameSuffix=""
if args.treeName!="outTree":
    isSys=True
    doDetails=False

    histDetail  =""
    jetDetail   ="kinematic clean"
    photonDetail="kinematic"

    nameSuffix="/sys"+args.treeName[7:]

c = Config()

commonsel={"m_mc"                     : args.is_MC,
           "m_debug"                  : False,
           "m_doDetails"              : doDetails,
           "m_histDetailStr"          : histDetail,
           "m_jetDetailStr"           : jetDetail,
           "m_photonDetailStr"        : photonDetail,
           "m_doPUReweight"           : True,
           "m_doCleaning"             : True,
           "m_jetPtCleaningCut"       : 25,
           "m_doTrigger"              : True
           }

#
# Process Ntuple
#

c.algorithm("MiniTreeEventSelection", { "m_name"                   : "",
                                     "m_debug"                  : False,
                                     "m_mc"                     : args.is_MC,
                                     "m_applyGRL"               : False,
                                     "m_doPUreweighting"        : False,
                                     "m_doTruthOnly"            : False,
                                     "m_triggerDetailStr"       : "passTriggers",
                                     "m_jetDetailStr"           : jetDetail,
                                     "m_photonDetailStr"        : photonDetail
                                     } )

triggers=[]
triggers.append(('HLT_g120_loose',130))
triggers.append(('HLT_g140_loose',150))

for trigger, photonPt in triggers:
    gammajet=commonsel.copy()
    gammajet.update({"m_name"         : "gammajet",
                     "m_trigger"      : trigger,
                     "m_photon0PtCut" : photonPt,
                     "m_jet0PtCut"    : 25})
    c.algorithm("GammaJetHistsAlgo", gammajet )

    gammajet_ystar=commonsel.copy()
    gammajet_ystar.update({"m_name"          : "gammajet_ystar",
                           "m_trigger"       : trigger,
                           "m_photon0PtCut"  : photonPt,
                           "m_jet0PtCut"     : 25,
                           "m_ystarCut"      : 0.6})
    c.algorithm("GammaJetHistsAlgo", gammajet_ystar )

